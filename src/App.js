import React, { Component } from 'react'
import PropTypes from 'prop-types'
//Components
import Header from './Components/NavBar/index.js'
import Content from './Components/Global/ContentComponent.js'
import Footer from './Components/Footer/ContenedorFooterComponent.js'
//Styles
import './App.sass'
import './Styles/_all.sass'
import 'bootstrap/dist/css/bootstrap.min.css'
import $ from 'jquery'

class App extends (Component) {
  static propTypes = {
    children: PropTypes.array.isRequired
  };

  constructor() {
    super();
    this.subir = React.createRef();   // Create a ref object 

    this.ocultarYmostrarAlHacerscroll = this.ocultarYmostrarAlHacerscroll.bind(this);
  }

  ocultarYmostrarAlHacerscroll() {
    $(document).ready(function () {

      $('#boton-ir-arriba').click(function () {
        $('body, html').animate({
          scrollTop: '0px'
        }, 300);
      });

      $(window).scroll(function () {

        if ($(this).scrollTop() > 0) {
          $('#boton-ir-arriba').slideDown(300);

        }
        else {
          $('#boton-ir-arriba').slideUp(300);
        }
      });
    });
  }



  render() {
    const { children } = this.props;

    return (
      <div className="App">

        <header className="fixed viewSearch" ref={this.subir}>
          <Header />
        </header>

        <div className="wrapper">
          <Content body={children} />
        </div>

        <footer className="nhfooter">
          <Footer scroll={this.ocultarYmostrarAlHacerscroll} subir={this.subir} />
        </footer>
      </div>
    );
  }
}

export default App
