import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import './BeneficiosComponent.sass'

export default class CreditoSearsBeneficiosComponent extends Component {

    render() {
        /** props recibidas desde el componente ContenedorCreditoComponent */
        const { datosCredito, imagenes } = this.props
        /** Obtiene el contenido del servicio : datos credito, obtiene la lista ordenada de beneficios  */
        const mapeadoDatosCredito = datosCredito.map((listaDatosCredito, i) => {
            return (
                (listaDatosCredito.is_children === false ?
                    <p key={i} className="texto-credito">{listaDatosCredito.li}</p>
                    : <Fragment key={i}>
                        <p className="texto-credito">{listaDatosCredito.li}</p>
                        <ul className="lista-credito">
                            {listaDatosCredito.p.map((lis, i) => {
                                return (
                                    <li key={i}><span>{lis.span}</span></li>
                                );
                            })}
                        </ul>
                    </Fragment>
                )
            );
        })

        return (
            <Fragment>
                {/* verifica si no carga la imagen para mostrar el spinner */}
                {imagenes != null
                    ?
                    <img src={imagenes} alt="slider-credito" className="img-fluid w-100"></img>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }

                <div className="contenedor-textos">
                    {mapeadoDatosCredito}
                </div>

                <div className="forma-pago">
                    <p>- Aceptada como forma de pago en:</p>
                    <ul>
                        <li>Hoteles Ostar <img alt="ostar-logo" src="https://medios-plazavip.sears.com.mx/credito-sears/smallostar2.jpg"></img></li>
                        <li>Restaurante y pastelería Sanborns</li>
                    </ul>
                </div>

            </Fragment>
        );
    }
}