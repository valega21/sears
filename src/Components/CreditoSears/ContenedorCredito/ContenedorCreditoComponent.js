import React, { Component } from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import CreditoBeneficios from '../Beneficios/BeneficiosComponent'
import CreditoPuntosSears from '../PuntosSears/PuntosSearsComponent'
import CreditoPreguntasFrecuentes from '../PreguntasContenedor/Preguntas/PreguntasComponent'
import CreditoCartaCumpleanios from '../PreguntasContenedor/Carta/CartaComponent'
import CreditoDescuento from '../PreguntasContenedor/Descuento/DescuentoComponent'
import CreditoCuponVip from '../PreguntasContenedor/Cupon/CuponComponent'
import { data } from './menuCredito.json'
import { data1 } from '../SolicitudCredito/solicitudCredito.json'
import { connect } from 'react-redux'
import { showCredito, showCreditoImg } from '../../../Redux/Actions/CreditoSears/actionCreditoSears'
import './ContenedorCreditoComponent.sass'
import Crequisitos from '../SolicitudCredito/RequisitosComponent'

class ContenedorCreditoComponent extends Component {
    constructor() {
        super();
        this.state = {
            data,
            data1,
            img: ""
        };
    }

    /** llama la inicialización de los metodos */
    componentDidMount() {
        this.props.showCredito()
        this.props.showCreditoImg()
    }

    render() {
        const { data, data1 } = this.state

        /** Obtiene la informacion del menu para la sección crédito */
        const mapeadoMenuCredito = data.content.map((item, i) => {
            const opcionesMenu = Object.values(item.options)

            return (
                <div className="list-group noborder" id="filtro-marcas" key={i}>
                    <h5 className="list-group-title">{item.title}</h5>

                    <div className="list-group-items">
                        {opcionesMenu.map((item, i) => {
                            return (
                                (item.target === true
                                    ? <Link key={i} className={item.is_bold === true ? "bold item" : "item"} to={item.url} target="_blank">{item.name}</Link>
                                    : <Link key={i} className={item.is_bold === true ? "bold item" : "item"} to={item.url}>{item.name}</Link>
                                )
                            )
                        }
                        )}
                    </div>
                </div>
            );
        })

        /** regresa el contenido de la seccion seleccionada */
        return (
            <div className="container">
                <div className="row">
                    <div className="second-level-navbar d-none d-lg-block col-md-3 col-xl-2">
                        {mapeadoMenuCredito}

                        <Link to="#!" target="_blank" rel="noopener noreferrer">
                            <img alt="atencion-en-linea" src="https://imgplaza.s3.amazonaws.com/CreditoSears/logo_Chat.jpg" />
                        </Link>
                    </div>

                    <div id="contenido-estatico" className="mt-3 col-lg-9 col-xl-10">
                        <Switch>
                            <Route exact path="/credito-sears/beneficios">
                                <CreditoBeneficios imagenes={this.props.imagen.imgWeb} datosCredito={this.props.datosCredito} />
                            </Route>

                            <Route exact path="/credito-sears/preguntas-frecuentes">
                                <CreditoPreguntasFrecuentes img={this.props.imagen.imgWeb} />
                            </Route>
                            <Route exact path="/credito-sears/descuento-primera-compra">
                                <CreditoDescuento img={this.props.imagen.imgWeb} />
                            </Route>
                            <Route exact path="/credito-sears/carta-cumpleanos">
                                <CreditoCartaCumpleanios img={this.props.imagen.imgWeb} />
                            </Route>
                            <Route exact path="/credito-sears/cupon-vip">
                                <CreditoCuponVip img={this.props.imagen.imgWeb} />
                            </Route>

                            <Route exact path="/credito-sears/puntos-sears">
                                <CreditoPuntosSears img={this.props.imagen.imgWeb} />
                            </Route>
                            <Route exact path="/credito-sears/requisitos-y-formato-de-solicitud">
                                <Crequisitos img={this.props.imagen.imgWeb} data={data1} />
                            </Route>
                        </Switch>
                    </div>

                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        datosCredito: state.credito.list,
        imagen: state.credito.list2
    }
}

export default connect(mapStateToProps, { showCredito, showCreditoImg })(ContenedorCreditoComponent)