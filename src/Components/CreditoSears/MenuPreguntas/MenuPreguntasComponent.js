import React, { Component } from 'react'
import './MenuPreguntasComponent.sass'

export default class MenuPreguntasComponent extends Component {
    /** renderiza la informacion del menu interno de preguntas frecuentes */
    render() {
        return (
            <div className="menuPreguntas">
                <h1 className="display-3 my-3">Restricciones para Cupones y Cartas Especiales de Crédito</h1>
                <ul className="lista-menu-preguntas">
                    <li className="display-4 my-3">
                        <a href="/credito-sears/descuento-primera-compra">Descuento Primera Compra</a>
                    </li>
                    <li className="display-4 my-3">
                        <a href="/credito-sears/carta-cumpleanos">Carta Cumpleaños</a>
                    </li>
                    <li className="display-4 my-3">
                        <a href="/credito-sears/cupon-vip">Cupón VIP</a>
                    </li>
                </ul>
            </div>
        );
    }
}