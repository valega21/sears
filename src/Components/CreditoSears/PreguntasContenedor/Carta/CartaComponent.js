import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import { data } from '../../creditoPreguntasFrecuentes.json'
import MenuPreguntasFrecuentesCredito from '../../MenuPreguntas/MenuPreguntasComponent.js'
import './CartaComponent.sass'

export default class CartaComponent extends Component {
    constructor() {
        super();
        this.state = {
            data
        }
    }

    render() {
        const { data } = this.state
        const { img } = this.props

        /**
         * obtiene la informacion de preguntas frecuentes
         */
        const mapeadoPreguntasFrecuentes = data.map((dat, i) => {
            const contenedor = Object.values(dat.content)
            return (
                <Fragment key={i}>
                    {dat.key === "birthday" ?
                        <Fragment>
                            <h1>{dat.title}</h1>
                            <div>
                                {contenedor.map((con, i) => {
                                    return (
                                        <Fragment key={i}>
                                            {con.is_bold === "true" ? <strong><p>{con.text}</p></strong> : <p>{con.text}</p>}
                                            <br />
                                        </Fragment>);
                                })}
                            </div>
                        </Fragment>
                        : null
                    }
                </Fragment>
            );
        })

        return (
            <div>
                {/* verifica si no carga la imagen para mostrar el spinner */}
                {img != null
                    ?
                    <img src={img} alt="slider-credito" className="img-fluid w-100"></img>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }

                {mapeadoPreguntasFrecuentes}
                
                <MenuPreguntasFrecuentesCredito />
            </div>
        );
    }
}