import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import { data } from '../../creditoPreguntasFrecuentes.json'
import MenuPreguntasFrecuentesCredito from '../../MenuPreguntas/MenuPreguntasComponent.js'

export default class PreguntasComponent extends Component {
    constructor() {
        super();
        this.state = {
            data
        }
    }

    render() {
        const { data } = this.state
        const { img } = this.props

        /** obtiene la información de preguntas frecuentes */
        const mapeadoPreguntasFrecuentes = data.map((dat, i) => {
            const content = Object.values(dat.content)
            return (
                <Fragment key={i}>{dat.key === "questions" ?
                    <Fragment>
                        <h1>{dat.title}</h1>
                        <div>
                            {content.map((con, i) => {
                                return (
                                    <Fragment key={i}>
                                        <h6>{con.subtitle}</h6>
                                        <p>{con.text}</p>
                                        <br />
                                    </Fragment>);
                            })}
                        </div>
                    </Fragment>
                    : null
                }
                </Fragment>
            );
        })

        return (
            <Fragment>
                {/* verifica si no carga la imagen para mostrar el spinner */}
                {img != null
                    ?
                    <img src={img} alt="slider-credito" className="img-fluid w-100"></img>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }
                {mapeadoPreguntasFrecuentes}
                <MenuPreguntasFrecuentesCredito />
            </Fragment>
        );
    }
}