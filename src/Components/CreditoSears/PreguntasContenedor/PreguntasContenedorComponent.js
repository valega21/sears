import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Spinner from 'react-bootstrap/Spinner'
import DescuentoCredito from './descuento.js'
import PreguntasFrecuentesCredito from './preguntas.js'
import CartaCumpleaniosCredito from './carta.js'
import CuponVipCredito from './cupon.js'

export default class PreguntasContenedorComponent extends Component {

    render() {
        const { img } = this.props

        /** renderiza el contenido de la seccion preguntas frecuentes seleccionada */
        return (
            <div>
                {/* verifica si no carga la imagen para mostrar el spinner */}
                {img != null
                    ?
                    <img src={img} alt="slider-credito" className="img-fluid w-100"></img>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }

                <Switch>
                    <Route exact path="/credito-sears/preguntas-frecuentes">
                        <PreguntasFrecuentesCredito />
                    </Route>
                    <Route exact path="/credito-sears/descuento-primera-compra">
                        <DescuentoCredito />
                    </Route>

                    <Route exact path="/credito-sears/carta-cumpleanos">
                        <CartaCumpleaniosCredito />
                    </Route>

                    <Route exact path="/credito-sears/cupon-vip">
                        <CuponVipCredito />
                    </Route>
                </Switch>
            </div>
        );
    }
}