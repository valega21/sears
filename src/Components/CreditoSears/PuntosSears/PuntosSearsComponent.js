import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import { data } from '../creditoPreguntasFrecuentes.json'
import './PuntosSearsComponent.sass'

export default class PuntosSearsComponent extends Component {
    constructor() {
        super();
        this.state = {
            data
        }
    }

    /** renderiza el contenido de la seccion puntos sears */
    render() {
        const { data } = this.state
        const { img } = this.props

        /** obtiene el contenido de la seccion preguntas frecuentes */
        const mapeadoPreguntasFrecuentes = data.map((dat, i) => {
            const subtituloPreguntas = Object.values(dat.content)
            return (
                (dat.key === "points" ?
                    <Fragment key={i}>
                        <h4>{dat.title}</h4>
                        {subtituloPreguntas.map((subtitulo, i) => {
                            const lista = Object.values(subtitulo.text)
                            return (
                                <Fragment key={i}>
                                    <h6>{subtitulo.subtitle}</h6>
                                    <br />
                                    <ul>
                                        {lista.map((lista, i) => {
                                            return (
                                                <li key={i}>{lista.li}</li>
                                            );
                                        }
                                        )}
                                    </ul>
                                    <span>{subtitulo.note}</span>
                                </Fragment>
                            );
                        })}
                    </Fragment>
                    : null
                )
            );
        })

        return (
            <div>
                {/* verifica si no carga la imagen para mostrar el spinner */}
                {img != null
                    ?
                    <img src={img} alt="slider-credito" className="img-fluid w-100"></img>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }
                <div className="contenedorPuntos">
                    {mapeadoPreguntasFrecuentes}
                </div>
            </div>
        );
    }
}