import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import { Link } from 'react-router-dom'
import './RequisitosComponent.sass'

export default class RequisitosComponent extends Component {

    render() {
        const { data, img } = this.props

        return (
            <Fragment>
                {/* verifica si no carga la imagen para mostrar el spinner */}
                {img != null
                    ?
                    <img src={img} alt="slider-credito" className="img-fluid w-100"></img>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }

                {data.map((informacion, i) => {
                    const listado = Object.values(informacion.content)

                    return (
                        <Fragment key={i}>
                            <p className="p-titulo"><strong> {informacion.title} </strong></p>
                            <p><strong> {informacion.subtitle} </strong></p>

                            <ul>
                                {listado.map((li, i) => {
                                    return (
                                        <li key={i}>{li.text}</li>
                                    );
                                })}
                            </ul>
                        </Fragment>

                    );
                })}

                <Link className="btn btn-sears link" to="../../documentos/DESCARGAR_SOLICITUD_DE_CREDITO_SEARS_MAYO_2018.pdf" target="_blank">
                    Descargar Solicitud de Crédito
                </Link>
            </Fragment>
        );
    }
}