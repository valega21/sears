import React, { Component, Fragment } from 'react'
import LowerFooter from './LowerFooter/LowerFooterComponent.js'
import Disclaimer from './Disclaimer/DisclaimerComponent.js'
import Mpagos from './MetodosPago/MetodosPagoComponent.js'
import Ufooter from './UpperFooter/UpperFooterComponent.js'
import NewsLetter from './NewsLetter/NewsLetterComponent.js'
import './ContenedorFooterComponent.sass'
import $ from 'jquery'

export default class ContenedorFooterComponent extends Component {
    constructor() {
        super();
        this.verBotonSubirPagina = this.verBotonSubirPagina.bind(this)
    }

    /**
     * función encargada de la visualizacion del boton subir pagina
     */
    verBotonSubirPagina() {
        var elemento = document.getElementById('boton-ir-arriba');
        const posicion = elemento.getBoundingClientRect();

        if (posicion.top < 570) {
            $(document).ready(function () {
                var myElement = $("#boton-ir-arriba");
                myElement.css("display", "none");
            });
        }
    }

    /**
     * verifica que llame la funcion solo si no es desde el login y el registro
     */
    componentDidMount() {
        if (window.location.pathname !== '/login' && window.location.pathname !== "/loginregistro") {
            this.verBotonSubirPagina();
        }
    }

    render() {
        const { scroll, subir } = this.props

        /** muetra el footer solo si no se encuentra en el login o en registro */
        if (window.location.pathname === '/login' || window.location.pathname === "/loginregistro") return null;
        return <Fragment>
            <NewsLetter />
            <Ufooter />
            <Mpagos />
            <Disclaimer />
            <LowerFooter />
            <button id="boton-ir-arriba" onClick={scroll(subir)}>.</button>
        </Fragment>;
    }
}