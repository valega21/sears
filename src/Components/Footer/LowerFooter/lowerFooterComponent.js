import React, { Component } from 'react'
import { data } from "./lower.json"
import './LowerFooterComponent.sass'
import { Link } from 'react-router-dom'

export default class LowerFooterComponent extends Component {

    constructor() {
        super()
        this.state = { data }
    }
    /**
     * renderiza terminos y condiciones e informacion de privacidad 
     */
    render() {
        /** obtiene la informacion de lowerFooter  */
        const mapeoLowerFooter = this.state.data.map((lower) =>
            (
                <Link to={lower.src} key={lower.title}>{lower.title} </Link>
            )
        )

        return (
            <div className="lowerfooter" >
                <div className="legales" >
                    {mapeoLowerFooter}
                </div>
            </div>
        );
    }
}