import React, { Component } from 'react'
import { data } from './metodos.json'
import './MetodosPagoComponent.sass'

export default class MetodosPagoComponent extends Component {

    constructor() {
        super();
        this.state = { data }
    }

    /** renderiza los metodos de pago utilizados por la empresa */
    render() {
        /** obtiene la informacion de los metodos de pago */
        const mapeoMetodosPago = this.state.data.map((pago) => {
            const imagenesMetodosPago = Object.values(pago.imgs);
            return (
                imagenesMetodosPago.map((ima, i) =>
                    (
                        <a href={ima.href} target="_blank" rel="noopener noreferrer" key={i}>
                            <img className="lazyload" src={ima.img} alt={ima.alt} />
                        </a>
                    )
                ))
        })

        return (
            <div className="nhmetodospago" >
                <div className="mplogos">
                    {mapeoMetodosPago}
                </div>
            </div>
        );
    }
}