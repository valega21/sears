import React, { Component, Fragment } from 'react'
import Modal from 'react-modal'
import './ModalBoletinComponent.sass'

//estilos aplicados al modal
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        width: '557px',//'40vw',
        height: '160px', //"25vh",
        transform: 'translate(-50%, -50%)',
        maxWidth: '1400px',
        maxHeight: '800px',
        backgroundColor: 'white',
        overflow: 'initial !important'
    }
}

export default class ModalBoletinComponent extends Component {

    render() {
        const { openModal, cerrarModal, handleClick, handleClick2, isSubmitting } = this.props
        
        /** regresa el modal al selecciona la opcion: hombre o mujer */
        return (
            <Fragment>
                <button onClick={handleClick} tabIndex="2" type="submit" id="suscription" name="suscription" className="btn-nh" disabled={isSubmitting}>Para Mujeres</button>
                <button onClick={handleClick2} tabIndex="3" type="submit" id="suscription2" name="suscription2" className="btn-nh" disabled={isSubmitting}>Para Hombres</button>

                <Modal isOpen={openModal} onRequestClose={cerrarModal} style={customStyles} aria-modal="true" ariaHideApp={false}>
                    <button title="close" onClick={cerrarModal} className="fancybox-item fancybox-close"> <span>&times;</span></button>
                    <div id="modal-newsletter">
                        <div className="check">
                            <span className="ico-check"></span>
                        </div>

                        <p>¡Listo! Recibirás las mejores promociones antes que nadie.</p>

                        <a href="/" className="closeNWL redbtn" title="close" onClick={cerrarModal} >Aceptar</a>
                    </div>
                </Modal>
            </Fragment>
        );
    }
}