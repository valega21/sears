import React, { Component, Fragment } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import './NewsLetterComponent.sass'
import ModalBoletinComponent from './ModalBoletinComponent'

export default class NewsLetterComponent extends Component {

    constructor() {
        super();
        this.state = {
            enviarM: false, // bandera para saber si selecciono mujer
            enviarH: false, // bandera para saber si selecciono hombre
            modalIsOpen: false // bandera para saber si el modal esta abierto
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleClick2 = this.handleClick2.bind(this);
    }

    //activa el envio de newsletter para mujeres
    handleClick() {
        this.setState(state => ({
            enviarM: true,
            enviarH: false
        }));
    }

    //activa el envio de newsletter para hombres
    handleClick2() {
        this.setState(state => ({
            enviarH: true,
            enviarM: false
        }));
    }

    /** funcion para abrir modal */
    openModal(ima) {
        this.setState({ modalIsOpen: true });
    }

    /** funcion para cerrar modal */
    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    /** renderiza el input con el correo suministrado, realiza las validaciones y si es correcto muestra el modal de confirmacion */
    render() {
        return (
            <div className="foterNewsletter">
                <div className="newsletterBlock">
                    <div className="nlMessage">
                        <h5>¡Regístrate a nuestro Newsletter!</h5>
                    </div>

                    <Formik
                        initialValues={{ newsletter: '' }}

                        validate={values => {
                            const errors = {};

                            if (!values.newsletter) {
                                errors.newsletter = 'La dirección de correo es requerida.';
                            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.newsletter)) {
                                errors.newsletter = 'Por favor ingresa un correo electrónico válido.';
                            }

                            return errors;
                        }}

                        onSubmit={(values, { setSubmitting }) => {
                            setTimeout(() => {
                                if (this.state.enviarM === true) {
                                    console.log("Envio mujeres...")
                                    this.openModal();

                                }
                                if (this.state.enviarH === true) {
                                    console.log("Envio hombres...")
                                    this.openModal();
                                }

                                console.log("Correo: ", values)
                                setSubmitting(false);
                            }, 400);
                        }}
                    >
                        {({ handleSubmit, isSubmitting }) => (
                            <Fragment>
                                <ErrorMessage name="newsletter" id="messageBoletin" className="ventana_emergente active erroresEmergen" component="div" />

                                <Form className="nlform" id="nlform" role="form" onSubmit={handleSubmit}>

                                    <Field tabIndex="1" type="text" id="newsletter" name="newsletter" placeholder="Tu correo electrónico" className="form-control :focus campo campo_texto required" />

                                    <ModalBoletinComponent
                                        openModal={this.state.modalIsOpen}
                                        cerrarModal={this.closeModal}
                                        handleClick={this.handleClick}
                                        handleClick2={this.handleClick2}
                                        isSubmitting={isSubmitting}
                                    />
                                </Form>
                            </Fragment>
                        )}
                    </Formik>
                </div>
            </div>
        );
    }
}