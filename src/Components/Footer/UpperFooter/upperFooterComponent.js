import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { data } from './upper.json'
import { data1 } from './siguenos.json'
import './UpperFooterComponent.sass'

export default class UpperFooterComponent extends Component {

    constructor() {
        super();
        this.state = { data, data1, active: '' }
    }

    /** renderiza la seccion nuestra compañia, dejanos ayudarte, politicas y contactos */
    render() {
        /**
         * mapeo para las secciones: nuestra compañia , politicas y dejanos ayudarte
         */
        const mapeoSecciones = this.state.data.map((secciones, i) => {
            const optiones = Object.values(secciones.options);
            return (
                <div id="div" className={secciones.class} key={i}>

                    <h4> {secciones.title} </h4>

                    <ul className="interno">
                        {optiones.map((op) =>
                            <li key={op.name}>
                                <a href={op.src}>{op.name}</a>
                            </li>
                        )}
                    </ul>
                </div>
            )
        })

        /**
         * mapeo para la seccion contacto
         */
        return (
            <div className="upperfooter">
                {mapeoSecciones}

                <div className="boxContacto">
                    {this.state.data1.map((contacto, i) => {
                        const redesSociales = Object.values(contacto.red);
                        return (
                            <Fragment key={i} >
                                <h4><Link to="/contactanos">{contacto.title} </Link></h4>
                                <div className="redesLogos">
                                    <span className="sigTitle">{contacto.subtitle}</span>
                                    <ul>
                                        {redesSociales.map((red, i) => (
                                            <li key={red.alt}>
                                                <a href={red.url} alt={red.alt}><i className={red.icon} /></a>
                                            </li>
                                        )
                                        )}
                                    </ul>
                                </div>

                                <div className="porEmail">
                                    <ul><li key={contacto.id}><a href="mailto:sears.internet@sears.com.mx">{contacto.text}</a></li></ul>
                                </div>
                                <div className="clima" key={contacto.id}>
                                    <iframe title="clima" frameBorder="0" allowtransparency="true" width="100%" height="100%" id={contacto.idClima} src={contacto.urlClima} />
                                </div>

                            </Fragment>
                        );
                    })}
                </div>
            </div>
        );
    }
}