import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ContentComponent extends Component {

  static propTypes = {
    body: PropTypes.array.isRequired
  };

  /** renderiza el contenido que se muestra en el cuerpo de la aplicacion */
  render() {
    const { body } = this.props;

    return (
      <div className="Content">
        {body}
      </div>
    );
  }
}