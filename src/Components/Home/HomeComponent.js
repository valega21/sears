import React, { Component, Fragment } from 'react'
import HomeComponenSection1 from './HomeComponentSection1/HomeComponentSection1.js'
import HomeComponentSection2 from './HomeComponentSection2/HomeComponentSection2.js'
import HomeComponentSection3 from './HomeComponentSection3/HomeComponentSection3.js'
import HomeComponentSection4 from './HomeComponentSection4/HomeComponentSection4.js'
import HomeComponentSection5 from './HomeComponentSection5/HomeComponentSection5.js'

class HomeComponent extends Component {
    render() {
        return (
            <Fragment>
                <section className="sec__slider">
                    <section className="mainSection">
                        <HomeComponenSection1 />
                    </section>
                </section>

                <section className="boxbannersOne">
                    <HomeComponentSection2 />
                    <HomeComponentSection3 />
                </section>
                <section className="carruselesNewH">
                    <HomeComponentSection4 />
                </section>

                <HomeComponentSection5 />
            </Fragment>
        );
    }
}

export default HomeComponent;