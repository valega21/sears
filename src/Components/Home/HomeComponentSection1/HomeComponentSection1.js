import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import Slider from 'react-slick'
import { connect } from 'react-redux'
import { showHomeSliderPrincipal } from '../../../Redux/Actions/home/actionHome'
import './HomeComponentSection1.sass'

const slider = {
    opacity: '1'
}

class HomeComponentSection1 extends Component {
    componentDidMount() {
        this.props.showHomeSliderPrincipal()
    }
    render() {
        var settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            adaptiveHeight: true,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            initialSlide: 0
        };

        const { slider1 } = this.props

        const mapeo = slider1.map((sli1, i) => {
            return (
                <Fragment key={i}>
                    <span className="slick-slide" data-slick-index={i + 1}>
                        <a href={sli1.url}>
                            <img alt="slider-principal" src={sli1.img} />
                        </a>
                    </span>
                    <button className="slick-next slick-arrow botonNext" type="button">Next</button>
                </Fragment>
            );
        })


        return (
            <Fragment>
                {slider1 != null
                    ?
                    <div className="magazineTop">
                        <div id="SliderHomePrincipal" className="contImgSlider slick-initialized slick-slider">
                            <div className="slick-list draggable" style={slider}>
                                <button className="slick-prev slick-arrow botonPrev" type="button">Previous</button>
                                <Slider {...settings}>
                                    {mapeo}
                                </Slider>
                            </div>
                        </div>
                    </div>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        slider1: state.home.list
    }
}

export default connect(mapStateToProps, { showHomeSliderPrincipal })(HomeComponentSection1)