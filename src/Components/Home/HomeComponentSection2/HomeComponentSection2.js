import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import { connect } from 'react-redux'
import { showHomeSliderSecundario } from '../../../Redux/Actions/home/actionHome'
import './HomeComponentSection2.sass'
import { Link } from 'react-router-dom'

class HomeComponentSection2 extends Component {

    componentDidMount() {
        this.props.showHomeSliderSecundario()
    }

    render() {
        const { slider2 } = this.props

        const mapeo = slider2.map((sli2, i) => {
            return (
                <Link to={"/producto/" + sli2.id + "/"} key={i}>
                    <img alt="imagenes-seccion2" src={sli2.img} />
                </Link>
            );
        })

        return (
            <Fragment>
                {mapeo != 0
                    ?
                    <section className="moduleTemplateImage">
                        <div className="container">
                            <div className="boxBan4">
                                {mapeo}
                            </div>
                        </div>
                    </section>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        slider2: state.home.list2
    }
}

export default connect(mapStateToProps, { showHomeSliderSecundario })(HomeComponentSection2)