import React, { Component, Fragment } from 'react'
import Spinner from 'react-bootstrap/Spinner'
import { connect } from 'react-redux'
import { showHomeSliderTres } from '../../../Redux/Actions/home/actionHome'
import './HomeComponentSection3.sass'
import { Link } from 'react-router-dom'

class HomeComponentSection3 extends Component {

    componentDidMount() {
        this.props.showHomeSliderTres()
    }

    render() {
        const { slider } = this.props

        const mapeo = slider.map((sli3, i) => (
            (sli3.widget === "simpleBanner" ?
                <Link to={"/producto/" + sli3.id + "/"} key={i} >
                    <img alt="banner-seccion3" src={sli3.img} />
                </Link>
                : null)
        ))

        return (
            <Fragment>
                {mapeo != 0
                    ?
                    <section className="moduleTemplateImage">
                        <div className="container">
                            <div className="boxBan2">
                                {mapeo}
                            </div>
                        </div>
                    </section>
                    :
                    <Spinner animation="border" variant="secondary" className="spinner">
                        <span className="sr-only">Cargando...</span>
                    </Spinner>
                }
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        slider: state.home.list3
    }
}

export default connect(mapStateToProps, { showHomeSliderTres })(HomeComponentSection3)