import React, { Component, Fragment } from 'react'
import './HomeComponentSection4.sass'

class HomeComponentSection4 extends Component {

    render() {
        return (
            <Fragment>
                <h2>Nuestras Recomendaciones</h2>

                <div className="rowProductos">
                    <div id="recomendacionesHomeSears20" className="contProductoSlider slick-initialized slick-slider">
                        <button className="slick-prev slick-arrow" type="button" style={{ display: 'block' }}>Previous</button>
                        <div className="slick-list draggable">
                            <div className="slick-track" style={{ opacity: '1', width: '100000', transform: 'translate3d(-270px, 0px, 0px)' }}>

                                <article className="productbox slick-slide slick-cloned">
                                    <div className="vistaRapida">
                                        <div className="contImgProd">
                                            <img alt="" src="" className="lazyload"></img>
                                        </div>
                                        <a className="linkProducto" href="#!">

                                        </a>
                                    </div>
                                    <div className="carruselContenido">
                                        <div className="descrip">
                                            <p></p>
                                        </div>
                                    </div>
                                </article>

                            </div>
                        </div>
                        <button className="slick-next slick-arrow" type="button" style={{ display: 'block' }}>Next</button>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default HomeComponentSection4