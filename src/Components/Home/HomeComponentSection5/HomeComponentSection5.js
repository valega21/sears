import React, { Component } from 'react'
import { data } from './section5.json'
import './HomeComponentSection5.sass'

class HomeComponentSection5 extends Component {

    constructor() {
        super();
        this.state = {
            data
        }
    }

    render() {
        return (
            <section className="boxbannersTwo">
                <section className="moduleTemplateImage">
                    <div className="container">
                        <div className="boxBan2">
                            {this.state.data.map((imagen, i) =>
                                <a key={imagen.id} href={imagen.href}>
                                    <img src={imagen.src} alt="imagen" className="lazyload" />
                                    <div className="mensageBoxBanner">
                                        <span></span>
                                    </div>
                                </a>
                            )}
                        </div>
                    </div>
                </section>
            </section>
        );
    }
}

export default HomeComponentSection5