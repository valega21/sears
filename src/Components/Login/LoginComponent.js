import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import './LoginComponent.sass'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as sessionActions from '../../Redux/Actions/Login/actionLogin'

class LoginComponent extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            checked: true,
            user: {
                email: '',
                password: ''
            }
        };
        this.handleCheck = this.handleCheck.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    handleCheck() {
        const check = this.state.checked;
        this.setState({ checked: !check });
        console.log("check:", this.state.checked)
    };

    onSubmit(history) {
        const { user } = this.state;
        const { login } = this.props.actions;
        login(user, history);
    }

    onChange(e) {
        const { value, name } = e.target.name;
        const { user } = this.state;
        user[name] = value;
        this.setState({ user });
    }

    render() {
        const { user: { email, password } } = this.state;
        console.log("usuario: ",this.state.user)
        const SubmitButton = withRouter(({ history }) => (
            <button
                className="redBtn"
                onClick={() => this.onSubmit(history)}
                type="submit">Iniciar Sesión
          </button>
        ));

        return (
            <div className="registro-usuario">
                <section className="registro__usuarios lb">
                    <h1> Iniciar Sesión</h1>

                    <div className="container">
                        <div className="panel panel-primary">
                            <div className="panel-body">
                                <div className="ventana_emergente active" id="loginMessage" style={{ display: 'none' }}>
                                    <div className="ico-alerta"></div>
                                </div>

                                <p>Inicia sesión para poder realizar tus compras.</p>
                            </div>

                            <Formik className="formik"
                                initialValues={{ email: '', password: '' }}
                                validate={values => {
                                    const errors = {};

                                    if (!values.email) {
                                        errors.email = 'La dirección de correo es requerida';
                                    } else if (
                                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                                    ) {
                                        errors.email = 'Dirección de Correo inválida.';
                                    }

                                    if (!values.password) {
                                        errors.password = 'La Contraseña es requerida';
                                    } else if (values.password && (values.password.length < 8)) {
                                        errors.password = 'La Contraseña debe ser mayor a 8 caracteres.';
                                    } else if (values.password && !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/i.test(values.password))) {
                                        errors.password = 'La contraseña debe contener al menos 1 letra minúscula. Debe contener al menos 1 letra mayúscula. Debe contener al menos 1 número.';
                                    }

                                    return errors;
                                }}

                                onSubmit={(values, { setSubmitting }) => {
                                    setTimeout(() => {
                                        // alert(JSON.stringify(values, null, 2));
                                        this.props.history.push('/');
                                        setSubmitting(false);
                                    }, 400);

                                    if (this.state.checked === true) {
                                        console.log("correo: ", values.email)
                                        console.log("pass: ", values.password)
                                    } else { console.log("no activo") }
                                }}

                            >
                                {({ isSubmitting }) => (
                                    <Form className="form-horizontal formulario">
                                        <input type="hidden" name="tkn_login" value="ac741ea5953b16722e7d134b32f30637d85df6d4" />
                                        <div className="form-group middle-left" style={{
                                            float: 'left',
                                            width: ' 48%'
                                        }}>
                                            <label>Email *</label>
                                            <Field type="email" name="email" placeholder="Ej: mail@mail.com" className="form-control :focus campo campo_texto required" />
                                            <ErrorMessage name="email" component="div" style={{ color: 'red' }} />
                                        </div>

                                        <div className="form-group middle-right" style={{
                                            float: 'right',
                                            width: ' 48%'
                                        }}>
                                            <label>Contraseña</label>
                                            <Field type="password" name="password" placeholder="" className="form-control campo campo_texto required" />
                                            <ErrorMessage name="password" component="div" style={{ color: 'red' }} />
                                        </div>

                                        <input type="hidden" name="clickRecoge" id="clickRecoge" />

                                        <div className="form-group">
                                            <div className="checkbox">
                                                <label>
                                                    <input onChange={this.handleCheck} defaultChecked={this.state.checked} type="checkbox" id="chk_terminos" name="promociones" className="required" />
                                                    <span>Recordar Contraseña.</span>
                                                </label>
                                                <a href="/recuperar" className="linkT">¿Olvidaste tu contraseña?</a>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            {/* <button type="submit" className="redBtn" disabled={isSubmitting}>Iniciar Sesión</button> */}
                                            <SubmitButton disabled={isSubmitting} />
                                        </div>
                                    </Form>
                                )}
                            </Formik>

                            <div className="panel-rs fullwidth"></div>
                            <div className="panel-rs">
                                <div className="btnRs">
                                    <a id="registrate" href="/loginregistro" className="btn-blanco">
                                        <b>Regístrate Aquí</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div >
        );
    }
}

const { object } = PropTypes;
LoginComponent.propTypes = {
    actions: object.isRequired
};

const mapDispatch = (dispatch) => {
    return {
        actions: bindActionCreators(sessionActions, dispatch)
    };
};

export default connect(null, mapDispatch)(LoginComponent);


