import React, { Component } from 'react'
import './RecuperarComponent.sass'

class RecuperarComponent extends Component {
    render() {
        return (
            <section>
                <div className="container">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h1>Recupera tu Contraseña</h1>
                        </div>
                        <div className="panel-body">
                            <div className="alert alert-warning1" role="alert">
                                Introduce tu correo electrónico registrado y recibirás un correo con las instrucciones para recuperar tu contraseña...
                            </div>

                            <form className="formulario" id="formulario" method="post">
                                <div className="form-group">
                                    <label className="col-sm-2 control-label" for="email">Email:</label>
                                    <input type="email" className="campo_texto required email" maxLength="100" placeholder="Escribe tu email" id="email" name="email" required />
                                </div>
                                <div className="form-group">
                                    <button className="btn-rojo" id="boton" type="submit">
                                        Recuperar Contraseña
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default RecuperarComponent