import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import './RegistroComponent.sass'

class RegistroComponent extends Component {
    render() {
        return (
            <div className="registro-usuario">
                <section className="registro__usuarios lb">
                    <h1>Crear Cuenta</h1>
                    <div className="container">
                        <div className="panel panel-primary">
                            <div className="panel-body">
                                <p>Regístrate en sears.com.mx es muy fácil y rápido.</p>
                                <Formik
                                    initialValues={{ email: '', celular: '', firstnameReg: '', lastnameMReg: '', lastnameReg: '', password: '', repassword: '' }}

                                    validate={values => {
                                        const errors = {};

                                        if (!values.email) {
                                            errors.email = 'Dirección de correo requerida';
                                        } else if (
                                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                                        ) {
                                            errors.email = 'Dirección de Correo inválida.';
                                        }

                                        if (!values.password) {
                                            errors.password = 'Contraseña requerida';
                                        } else if (values.password && !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/i.test(values.password))) {
                                            errors.password = 'La contraseña debe contener al menos 1 letra minúscula. Debe contener al menos 1 letra mayúscula. Debe contener al menos 1 número y una logitud de al menos 8 caracteres.'
                                        }

                                        if (!values.repassword) {
                                            errors.repassword = 'Confirmación de contraseña requerida';
                                        } else if (values.repassword !== values.password) {
                                            errors.repassword = 'Las contraseñas deben coincidir.';
                                        }
                                        if (!values.firstnameReg) {
                                            errors.firstnameReg = 'Nombre obligatorio.';
                                        } else if (values.firstnameReg && (/^[0-9]$/i.test(values.firstnameReg))) {
                                            errors.firstnameReg = 'El nombre no debe contener números.';
                                        }

                                        if (!values.lastnameReg) {
                                            errors.lastnameReg = 'Apellido paterno obligatorio.';
                                        } else if (values.lastnameReg && (/^[0-9]$/i.test(values.lastnameReg))) {
                                            errors.lastnameReg = 'El apellido no debe contener números.';
                                        }

                                        if (values.celular && (values.celular.length < 10)) {
                                            errors.celular = 'Número de teléfono debe contener 10 dígitos.';
                                        } else if (values.celular && (values.celular.length > 10)) {
                                            errors.celular = 'Número de teléfono debe contener 10 dígitos.';
                                        }

                                        if (values.lastnameMReg && (/^[0-9_]*$/i.test(values.lastnameMReg))) {
                                            errors.lastnameMReg = 'El apellido materno no debe contener números';
                                        }

                                        return errors;
                                    }}
                                    onSubmit={(values, { setSubmitting }) => {
                                        setTimeout(() => {
                                            alert(JSON.stringify(values, null, 2));
                                            setSubmitting(false);
                                        }, 400);
                                    }}
                                >
                                    {({ isSubmitting }) => (
                                        <Form role="form" id="formRegister" name="formRegister" className="form-horizontal formulario" onSubmit={this.handleSubmit}>
                                            <input type="hidden" value="registrarse" id="accion" name="accion" />
                                            <div id="registrationMessage" className="ventana_emergente active erroresEmergen" style={{ display: "none" }}>&nbsp;</div>

                                            <div className="form-group middle-left">
                                                <label>Email *</label>
                                                <Field tabIndex="1" id="email" name="email" placeholder="Ej: nombre@mail.com" className="form-control :focus campo_texto campo required" />
                                                <ErrorMessage name="email" component="div" style={{ color: 'red' }} />
                                            </div>
                                            <div className="form-group middle-right">
                                                <label>Teléfono Celular</label>
                                                <Field tabIndex="2" type="text" id="celular" name="celular" placeholder="" maxLength="15" className="form-control campo campo_texto" />
                                                <ErrorMessage name="celular" component="div" style={{ color: 'red' }} />
                                            </div>
                                            <div className="form-group">
                                                <label>¡sin acentos ni caracteres especiales!</label>
                                            </div>
                                            <div className="form-group middle-left">
                                                <label>Nombre *</label>
                                                <Field tabIndex="3" type="text" pattern="[A-Za-z ]+" id="firstnameReg" name="firstnameReg" placeholder="Ej: Eduardo" className="form-control :focus campo campo_texto required" />
                                                <ErrorMessage name="firstnameReg" component="div" style={{ color: 'red' }} />
                                            </div>
                                            <div className="form-group middle-right">
                                                <label>Apellido Paterno *</label>
                                                <Field tabIndex="4" type="text" pattern="[A-Za-z ]+" id="lastnameReg" name="lastnameReg" placeholder="Ej: Ramirez" className="form-control campo campo_texto required" />
                                                <ErrorMessage name="lastnameReg" component="div" style={{ color: 'red' }} />
                                            </div>
                                            <div className="form-group middle-left">
                                                <label>Apellido Materno</label>
                                                <Field tabIndex="5" type="text" pattern="[A-Za-z ]+" id="lastnameMReg" name="lastnameMReg" placeholder="Ej: Ramirez" className="form-control campo campo_texto" />
                                                <ErrorMessage name="lastnameMReg" component="div" style={{ color: 'red' }} />
                                            </div>
                                            <div className="form-group middle-right">
                                                <label>Contraseña</label>
                                                <Field tabIndex="6" type="password" id="password" name="password" placeholder="Contraseña *" className="form-control campo campo_texto required required" />
                                                <ErrorMessage name="password" component="div" style={{ color: 'red' }} />
                                            </div>
                                            <div className="form-group middle-left">
                                                <label>Confirma Contraseña *</label>
                                                <Field tabIndex="7" type="password" id="repassword" name="repassword" placeholder="Confirmar Contraseña *" className="form-control campo campo_texto required" />
                                                <ErrorMessage name="repassword" component="div" style={{ color: 'red' }} />
                                            </div>

                                            <input type="hidden" name="redirect" id="redirect" value="/"></input>
                                            <input type="hidden" name="link_referido" id="link_referido"></input>

                                            <div className="form-group secondary valid">
                                                <label> * Campos obligatorios</label>
                                            </div>
                                            <div className="form-group secondary valid">
                                                <p>Para tus compras y consultas de Tarjeta Sears es necesario que cuentes con tu NIP, si aún no lo tienes comunícate al 01 800 900 2123.</p>
                                            </div>
                                            <div className="form-group secondary">
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" tabIndex="8" id="chk_terminos" name="promociones" className="required" defaultChecked/>
                                                        <span>Deseo recibir promociones exclusivas por email.</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="form-group secondary"></div>
                                            <div className="form-group secondary">
                                                <div className="checkbox">
                                                    <input type="checkbox" tabIndex="9" id="chk_terminos_politica" name="chk_terminos_politica" className="requires" defaultChecked style={{ display: "none" }} />
                                                    <label>
                                                        <span>
                                                            Al registrarte aceptas los
                                                    <a href="/terminos-y-condiciones" target="_blank" className="linkT"> Términos y Condiciones </a>
                                                            y la
                                                    <a href="/Aviso_Privacidad" target="_blank" className="linkT"> Política de Privacidad </a>
                                                            de SEARS
                                                </span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <button tabIndex="10" type="submit" id="register" name="registrar" className="btn-rojo">Crear Cuenta</button>
                                            </div>

                                            <div className="panel-rs fullwidth"></div>

                                            <div className="panel-rs">
                                                <div className="btnRs">
                                                    <a href="/login" className="btn-blanco">Inicia Sesión Aquí</a>
                                                </div>
                                            </div>

                                            <div style={{ display: "none" }}>
                                                <div id="loginPluginDiv">
                                                    Loading Social Login...
                                        </div>
                                            </div>

                                        </Form>
                                    )}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
export default RegistroComponent