import React, { Component, Fragment } from 'react';

class NavCreditoComponent extends Component {

    render() {
        return (
            <Fragment>
                <a href="#!"> Crédito </a>
                <div className="contentCredito">
                    <ul>
                        <li className="alone"><a href="/credito-sears/beneficios">Beneficios</a></li>
                        <li className="alone"><a href="#!">Solicitud de Crédito</a></li>
                        <li className="alone"><a href="#!">Registro de Tarjetas Sears</a></li>
                        <li className="alone"><a href="#!">Consulta Estado de Cuenta, Saldo y Pago en Línea</a></li>
                    </ul>
                </div>
            </Fragment>
        );
    }
}
export default NavCreditoComponent;