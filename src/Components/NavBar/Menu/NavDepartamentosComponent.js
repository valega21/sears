import React, { Component } from 'react';

class NavDepartamentosComponent extends Component {

    render() {
        const { data } = this.props

        return (
            <ul>
                <li className="productosNewCat hide">
                    <a href="#!">Departamentos</a>
                    <div className="menuHeader">
                        <div className="subMenu">

                            {data.map((category, index) => {
                                const sub = Object.values(category.subcategories);
                                return (
                                    <dl key={index}>
                                        <dt className=" ">
                                            <a className="" href="#!">
                                                {category.title}
                                            </a>
                                            <dl>
                                                <dt>
                                                    <dl>
                                                        {sub.map((subcategoria, index) => {
                                                            const opc = Object.values(subcategoria.options);
                                                            return (
                                                                <dt className="more" key={index}>
                                                                    <a href="#!">{subcategoria.title}</a>
                                                                    <ul>
                                                                        {opc.map((opcion, i) => {
                                                                            return (
                                                                                <li className="alone" href="#" key={i}>
                                                                                    <a href="#!">{opcion.name}</a>
                                                                                </li>
                                                                            );
                                                                        })}
                                                                    </ul>
                                                                </dt>
                                                            );
                                                        }
                                                        )}
                                                    </dl>
                                                </dt>
                                            </dl>
                                        </dt>
                                    </dl>
                                );
                            })}
                        </div>
                    </div>
                </li>
            </ul>
        );
    }
}
export default NavDepartamentosComponent;