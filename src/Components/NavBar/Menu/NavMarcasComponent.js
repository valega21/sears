import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { showMarcas } from '../../../Redux/Actions/actionMarcas.js';

class NavMarcasComponent extends Component {

    componentDidMount() {
        this.props.showMarcas();
    }

    render() {
        const { marcas } = this.props

        return (
            <li className="menuMarcas alone">
                <span> Nuestras Marcas </span>
                <div className="contentMarcas">
                    <div className="marcasInt">
                        {marcas.map((tipo, i) => {
                            const opciones = Object.values(tipo.content);
                            return (
                                <Fragment key={i}>
                                    <h4>{tipo.title}</h4>
                                    <ul>
                                        {opciones.map((opc, i) => (
                                            (opc.id === 3451
                                                ? <li className="alone" key={i}><a href="/tienda/3451/pier-1-imports"><img alt="" src={opc.img}></img></a></li>
                                                : <li className="alone" key={i}><a href="#!"><img alt="" src={opc.img}></img></a></li>)
                                            // <li className="alone" key={i}><a href="#!"><img src={opc.img}></img></a></li>
                                        ))}
                                    </ul>
                                </Fragment>
                            );
                        }
                        )}

                    </div>
                </div>
            </li>
        );
    }
}

function mapStateToProps(state) {
    return {
        marcas: state.marca.list
    }
}

export default connect(mapStateToProps, { showMarcas })(NavMarcasComponent);