import React, { Component, Fragment } from 'react';

class NavMesaRegalosComponent extends Component {

    render() {
        return (
            <Fragment>
                <a href="https://wap-mesa-regalos.sears.com.mx/"> Mesa de Regalos </a>
                <div className="contentMesa">
                    <ul>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=1">Bautizo</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=2">Boda</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=3">Aniversario</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=6">Primera Comunión</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=7">Despedida de Soltera</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=10">Baby Shower</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=11">Quince Años</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=12">Cumpleaños</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=13">Graduación</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=14">Primera Casa</a></li>
                        <li className="alone"><a href="https://wap-mesa-regalos.sears.com.mx/buscar?tipo=15">Mi Día</a></li>
                    </ul>
                </div>
            </Fragment>
        );
    }
}
export default NavMesaRegalosComponent;