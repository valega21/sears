import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { showMarcas } from '../../../Redux/Actions/actionMarcas.js';
import './NavResponsive.sass';

class NavResponsiveComponent extends Component {

    render() {
        const { data, marcas } = this.props
        return (
            <nav className="navegacionM">
                <dl className="navProductos" id="navProductos">
                    <dt>Departamentos</dt>
                    <dd>
                        <dl>
                            {data.map((tipo, i) => {
                                const sub = Object.values(tipo.subcategories);
                                return (
                                    <Fragment key={i}>
                                        <dt id={i}>
                                            <a href="#!">{tipo.title}</a>
                                        </dt>
                                        <dd>
                                            <ul>
                                                {sub.map((subcategoria, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <a href="#!">{subcategoria.title}</a>
                                                        </li>
                                                    );
                                                }
                                                )}
                                            </ul>
                                        </dd>
                                    </Fragment>
                                );
                            }
                            )}
                        </dl>
                    </dd>
                </dl>

                <dl id="navRecomendados" className="navRecomendados">
                    <dt>Nuestras Marcas</dt>
                    {marcas.map((tipo, i) => {
                        const opciones = Object.values(tipo.content);
                        return (
                            <Fragment key={i}>
                                <dd> {tipo.id !== 2 ? <dt className="dt-title">{tipo.title}</dt> : null}
                                    <dl>
                                        {opciones.map((opc, i) => (

                                            <dt className="alone" key={i}>
                                                <a href="#!">
                                                    {opc.name}
                                                </a>
                                            </dt>
                                        ))}
                                    </dl>
                                </dd>
                            </Fragment>
                        );
                    }
                    )}
                </dl>

                <dl id="mesa" className="navSears">
                    <dt>
                        <a href="https://wap-mesa-regalos.sears.com.mx/" target="_blank" rel="noopener noreferrer">Mesa de Regalos <i className="fa fa-chevron-right"></i></a>
                    </dt>
                </dl>

                <dl id="navRecomendados1" className="navRecomendados">
                    <dt>Crédito</dt>
                    <dd>
                        <dl>
                            <dt><a href="#!">Registro de tarjetas Sears</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Consulta tu Estado de Cuenta, Saldo y Pago en línea</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Solicitud de Crédito en Línea</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Consultar Estado de Solicitud de Crédito</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Requisitos y Formato de Solicitud</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Contrato de Crédito Sears - Carátula</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="/credito-sears/beneficios">Beneficios</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Puntos Sears</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Directorio de Despachos de Cobranza</a></dt>
                        </dl>

                        <dl>
                            <dt><a href="#!">Preguntas Frecuentes</a></dt>
                        </dl>
                    </dd>
                </dl>

                <dl className="navSears">
                    <dt>
                        <a href="#!">Recoger en Tienda <i className="fa fa-chevron-right"></i></a>
                    </dt>
                </dl>

                <dl className="navSears">
                    <dt>
                        <a href="/localizador-tiendas">Tiendas
                        <i className="fa fa-chevron-right"></i>
                        </a>
                    </dt>
                </dl>

                <dl id="navRecomendados2" className="navRecomendados">
                    <dt>Servicios</dt>
                    <dd>
                        <dl>
                            <dt><a href="/auto-center">Sears AutoCenter</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="http://searsserviceteam.com.mx/">Servicio Técnico</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="https://www.viajesears.com/" target="_blank" rel="noopener noreferrer">Viajes Sears</a></dt>
                        </dl>
                        <dl>
                            <dt><a href="/contactanos">Contacto</a></dt>
                        </dl>
                    </dd>
                </dl>

            </nav>
        );
    }
}

function mapStateToProps(state) {
    return {
        marcas: state.marca.list
    }
}

export default connect(mapStateToProps, { showMarcas })(NavResponsiveComponent);