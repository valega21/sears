import React, { Component } from 'react';

class NavTiendasServiciosComponent extends Component {

    render() {
        return (
            <ul >
                <li className="tiendas alone" style={{ width: "50%" }} >
                    < a href="/localizador-tiendas" > Tiendas </a>
                </li >
                <li className="mall alone" style={{ width: "50%" }} >
                    < a href="#!" > Servicios </a>
                    <div className="contentServicios">
                        <ul>
                            <li className="alone"><a href="/auto-center">AutoCenter</a></li>
                            <li className="alone"><a href="http://searsserviceteam.com.mx/">Servicio Técnico</a></li>
                            <li className="alone"><a href="https://www.viajesears.com/">Viajes Sears</a></li>
                            <li className="alone"><a href="contactanos">Contáctanos</a></li>
                        </ul>
                    </div>
                </li >
            </ul>
        );
    }
}
export default NavTiendasServiciosComponent;