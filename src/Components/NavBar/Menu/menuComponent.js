import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { data } from './Categorias.json';

import NavTiendasServicios from './NavTiendasServiciosComponent.js';
import NavMesaRegalos from './NavMesaRegalosComponent.js';
import NavCredito from './NavCreditoComponent.js';
import NavResponsive from './NavResponsiveComponent.js';
import NavMarcas from './NavMarcasComponent.js';
import NavDepartamentos from './NavDepartamentosComponent.js';
import './NavInferior.sass';

class MenuComponent extends Component {

    constructor() {
        super();
        this.state = {
            data
        };
    }

    render() {
        const { data } = this.state

        return (
            <Fragment>
                <section className="header__movil view">
                    <NavResponsive data={data} />
                </section>
                <div className="header__inferior" >


                    <div className="container" >
                        <nav className="productosHead" id="productosHead">
                            <NavDepartamentos data={data} />
                        </nav>

                        <nav className="promoHeader" >
                            <ul>
                                <NavMarcas />

                                <li className="menuMesa alone" >
                                    <NavMesaRegalos />
                                </li>

                                <li className="menuCredito alone " >
                                    <NavCredito />
                                </li>

                                <li className="alone" >
                                    < Link to=""> Recoger en Tienda </Link>
                                </li >
                            </ul>
                        </nav>

                        <nav className="menuTiendas noSubMenu" >
                            <NavTiendasServicios />
                        </nav>
                    </div>
                </div >
            </Fragment>
        );
    }
}

export default MenuComponent;