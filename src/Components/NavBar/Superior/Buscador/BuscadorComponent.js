import React, { Component } from 'react';

class BuscadorComponent extends Component {
    render() {
        return (
            <div className="search">
                <section className="navbar-form navbar-left hpadding0 hmargecontenidozul">
                    <div className="contSearch suggestSearch">
                        <div className="input2">
                            <input id="selectCategoriasMenu" type="hidden" />
                            <input id="search" type="search" placeholder="¿Qué es lo que buscas?" style={{paddingRight:"5px"}} className="ui-autocomplete-input" autoComplete="off" />
                            <button className="bt__search" type="button"></button>
                            <div id="recentSearch">
                                <h5>Búsquedas recientes</h5>
                            </div>
                            <div className="suggestions"></div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default BuscadorComponent;