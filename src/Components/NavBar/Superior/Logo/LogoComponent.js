import React, { Component, Fragment } from 'react'

class LogoComponent extends Component {

    render() {
        return (
            <Fragment>
                <a href="/" title="Ir a la página principal de SEARS" className="header__logo">
                    <div className="hidden" style={{ width: "1px", height: "1px", overflow: "hidden" }}>Página Oficial tiendas Sears.</div>
                </a>
                <div id="header__movil" className="desktopMenu movil">

                    <a href="#!" className="icon" >
                        <div className="hidden" style={{ width: "1px", height: "1px", overflow: "hidden" }}>Página Oficial tiendas Sears.</div>
                    </a>
                </div>
            </Fragment>
        );
    }
}

export default LogoComponent