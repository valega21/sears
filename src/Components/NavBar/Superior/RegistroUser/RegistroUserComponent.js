import React, { Component } from 'react'

class RegistroUserComponent extends Component {

    constructor() {
        super();
        this.state = {
            hover: false
        };

        this.hoverOn = this.hoverOn.bind(this)
        this.hoverOff = this.hoverOff.bind(this)
    }

    hoverOn() {
        this.setState({ hover: true });
    }
    hoverOff() {
        this.setState({ hover: false });
    }

    render() {
        return (
            <nav className="menuPrincipal menuAnim">
                <ul>
                    <li className="buscar"><a href="#!">Buscar</a></li>

                    <li className="ayuda">
                        <a href="!#"><span>Obtén</span> Ayuda</a>

                        <div className="box__ayuda">
                            <dl>
                                <dt className="icoLlam">Llámanos</dt>
                                <dd>
                                    <a href="tel:018008368246">01800-836-8246</a> Dentro de la República Mexicana.
                                </dd>
                            </dl>

                            <dl>
                                <dd className="icoCha">
                                    <span className="btn-chatZ">Chat</span>
                                </dd>
                            </dl>
                            <dl>
                                <dd className="icoEma">
                                    <a href="mailto:sears.internet@sears.com.mx">Escríbenos</a>
                                </dd>
                            </dl>
                        </div>
                    </li>

                    <li className={this.state.hover ? "login" : "login log"} //data-toggle="dropdown"
                        onMouseEnter={this.hoverOn}
                        onMouseLeave={this.hoverOff}>
                        <a href="!#" id="userInformation"><span>Bienvenido</span> Regístrate</a>

                        <div className="box__login dropdown-menu" id="box__login">
                            <dl className="logIn">
                                <dd>
                                    <a href="/login" className="redBtn btn-login">Ingresar</a>
                                </dd>
                                <dd>
                                    <p className="textoNuevo" style={{ top: "10px", position: "relative" }}>¿Eres Nuevo?</p>
                                    <a href="/loginregistro" className="standardBtn btn-registro">Regístrate</a>
                                </dd>
                            </dl>
                        </div>
                    </li>

                    <li className="wishlist_snippet">
                        <input type="hidden" id="logeado" name="logeado"></input>
                    </li>

                    <li className="cart notification cartPreview dropdown-toggle" data-toggle="dropdown">
                        <a href="!#">
                            <div className="alinear2">
                                <div className="c2">
                                    <span className="push c2" id="cart_count">0</span>
                                    <span>Mi bolsa de</span> Compras
                                </div>
                            </div>
                        </a>
                        <div className=" box__carrito dropdown-menu" >
                            <h3>Mi Bolsa</h3>
                            <div className="boxCarritoCont dropdown-item" ></div>
                            <div className="botones_cajaPago dropdown-item">
                                <div className="botones">
                                    <a href="!#" className="botonComprarClick dropdown-item">
                                        Ir a la bolsa
                                    </a>
                                    <a href="!#" className="botonComprarAhora dropdown-item">
                                        Comprar ahora
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default RegistroUserComponent