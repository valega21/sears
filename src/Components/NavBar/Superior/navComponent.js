import React, { Component } from 'react'
import Logo from './Logo/LogoComponent'
import Buscador from './Buscador/BuscadorComponent'
import Registro from './RegistroUser/RegistroUserComponent'
import './NavSuperior.sass'

class NavComponent extends Component {

    render() {
        if (window.location.pathname === '/login' || window.location.pathname === '/loginregistro')
            return (
                <div className="header__middle ">
                    <div className="container ">
                        <Logo />
                    </div>
                </div>

            );
        return <div className="header__middle ">
            <div className="container ">
                <Logo />

                <Buscador />

                <Registro />

            </div>
        </div>;
    }
}

export default NavComponent