import React, { Component, Fragment } from 'react'
import Nav from './Superior/NavComponent'
import Menu from './Menu/MenuComponent'

class index extends Component {
    render() {
        if (window.location.pathname === '/login' || window.location.pathname === '/loginregistro')
            return <Fragment>
                <Nav />
            </Fragment>;

        return <Fragment>
            <Nav />
            <Menu />
        </Fragment>;
    }
}

export default index