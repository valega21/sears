import React, { Component } from 'react'
import ModalClickRecoge from './ModalClickRecogeComponent'

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'initial'
}

export default class BotonesComponent extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            price: "",
            imgs: [],
            tall: ''
        }
    }
    
    render() {
        const { nameProduct, price, imgs, productos, colorSeleccionado2, tallaSeleccionada2, tipo, general } = this.props

        return (
            <form onClick={this.cambioTalla} style={parseInt(tipo) !== 10 ? ver : ocultar} >
                <div className="fieldset">
                    <button id="comprar" className={general.stock === 0 ? " disabledComprar" : "btn-compra2"} type="submit" disabled={parseInt(general.stock) === 0 ? true : false}>Comprar Ahora</button>
                    <button id="addCarrito" href="#!" className={general.stock === 0 ? "disabledBolsa" : "btn-carrito slidcar"} type="button" disabled={parseInt(general.stock) === 0 ? true : false}>Agregar a bolsa</button>

                    <ModalClickRecoge
                        name={nameProduct} price={price} imgs={imgs} productos={productos}
                        colorSeleccionado2={colorSeleccionado2} tallaSeleccionada2={tallaSeleccionada2} general={general}
                    />
                </div>

                <div className="mensajeAgotado" id="mensajeAgotado" style={general.stock === 0 ? ver : ocultar}>
                    <div className="alertaAgotado">
                        <span>Producto Agotado</span>
                    </div>
                </div>
            </form>
        );
    }
}