import React, { Component, Fragment } from 'react';

export default class BoxTallaComponent extends Component {

    constructor() {
        super();
        this.state = {
            value: ' '
        }
        this.talla = this.talla.bind(this)
    }

    talla(event) {
        this.setState({ value: event.target.value })
        this.props.seleccionTallaColor(this.state.value)
    }

    render() {
        const { colores, productos } = this.props;
        console.log("talla seleccionada: ", this.state.value)
        return (
            <div className="boxTallas">
                <span>Tallas</span>
                <div className="opc_sizes">
                    <select name="talla" id="talla" value={this.state.value} onChange={this.talla}>
                        {productos.map((tal) => {
                            const talla2 = Object.values(tal.sizes)
                            return (talla2.map((ta, it) => {
                                return (
                                    (colores === tal.color ?
                                        <Fragment key={it}>
                                            < option value={ta.size} > {ta.size}</option>
                                        </Fragment> :
                                        null)
                                );
                            }
                            ));
                        })}
                    </select>
                </div>
            </div>
        );
    }
}