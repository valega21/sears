import React, { Component } from 'react'
import $ from 'jquery'
import "chosen-js/chosen.css"
import "chosen-js/chosen.jquery.js"

export default class Chosen extends Component {
    constructor() {
        super();
        this.state = {
            event: 0
        };
        this.handleChange = this.handleChange.bind(this)
    }
    
    componentDidMount() {
        this.$el = $(this.el);
        this.$el.chosen();

        this.handleChange = this.handleChange.bind(this);
        this.$el.on('change', this.handleChange);
    }

    componentWillUnmount() {
        this.$el.off('change', this.handleChange);
        this.$el.chosen('destroy');
    }

    componentDidUpdate(prevProps) {
        if (prevProps.children !== this.props.children) {
            this.$el.trigger("chosen:updated");
        }
    }

    handleChange(event) {
        this.setState({ event: event.target.value });
        this.props.event(this.state.event)
    }

    render() {
        const { data } = this.props
        return (
            <div style={{ position: 'relative', bottom: '20px', with: "auto" }}>
                <select className="Chosen-select" ref={el => this.el = el} value={this.state.value} onChange={this.handleChange}>
                    <option value="-1">Ver todas las tiendas</option>
                    {data.map((tiendas) => {
                        return (
                            <option key={tiendas.id} value={tiendas.id}>{tiendas.tienda}</option>
                        )
                    }
                    )}
                </select>
            </div>
        );
    }
}