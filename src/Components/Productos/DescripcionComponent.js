import React, { Component, Fragment } from 'react'
import DescripcionDescripcion from './DescripcionDescripcionComponent'
import DescripcionPoliticas from './DescripcionPoliticasComponent'
import DescripcionSucursales from './DescripcionSucursales'
import DescripcionPagos from './DescripcionPagosComponent'

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'block'
}

const verSucrusal = {
    display: 'initial'
}

export default class DescripcionComponent extends Component {
    render() {
        const { general, pagos, politicas, atributos, tipo, disponibilidad, isToggleOn,
            mostrarSucursales, mostrarDescripcion, mostrarPoliticas, mostrarPagos,
            activarD, activarP, activarPagos, activarS, sucursal } = this.props

        return (
            <div className="container">
                <ul className="viewMenu">
                    {(isToggleOn === true && activarD === false && activarP === false)
                        ? <Fragment>
                            <li className="laDescrip " onClick={(e) => mostrarDescripcion(e)}>
                                <span>Descripción</span>
                            </li>

                            <li id="btnProdSucursales" className="prodSucursales select" style={(parseInt(tipo) === 10) ? ver : ocultar} onClick={(e) => mostrarSucursales(e)}>
                                <span>Sucursales</span>
                            </li>

                            <li className="returnPolicy" onClick={(e) => mostrarPoliticas(e)}>
                                <span>Políticas de garantía y devolución</span>
                            </li>

                            <li className="anchorFormas" style={(parseInt(tipo) !== 10) ? ver : ocultar} onClick={(e) => mostrarPagos(e)}>
                                <span>Formas de Pago</span>
                            </li>
                        </Fragment>
                        : (!isToggleOn === true && activarD === true
                            ? <Fragment>
                                <li className={(activarD === true) ? "laDescrip select" : "laDescrip "} onClick={(e) => mostrarDescripcion(e)}>
                                    <span>Descripción</span>
                                </li>

                                <li id="btnProdSucursales" className={(activarS === true) ? "prodSucursales " : "prodSucursales"} style={(parseInt(tipo) === 10) ? ver : ocultar} onClick={(e) => mostrarSucursales(e)}>
                                    <span>Sucursales</span>
                                </li>

                                <li className={(activarP === true) ? "returnPolicy " : "returnPolicy"} onClick={(e) => mostrarPoliticas(e)}>
                                    <span>Políticas de garantía y devolución</span>
                                </li>

                                <li className={activarPagos === true ? "anchorFormas" : "anchorFormas"} style={(parseInt(tipo) !== 10) ? ver : ocultar} onClick={(e) => mostrarPagos(e)}>
                                    <span>Formas de Pago</span>
                                </li>
                            </Fragment>
                            : (!isToggleOn === true && activarP === true
                                ? <Fragment>
                                    <li className={(activarD === true) ? "laDescrip " : "laDescrip "} onClick={(e) => mostrarDescripcion(e)}>
                                        <span>Descripción</span>
                                    </li>

                                    <li id="btnProdSucursales" className={(activarS === true) ? "prodSucursales" : "prodSucursales"} style={(parseInt(tipo) === 10) ? ver : ocultar} onClick={(e) => mostrarSucursales(e)}>
                                        <span>Sucursales</span>
                                    </li>

                                    <li className={(activarP === true) ? "returnPolicy select" : "returnPolicy"} onClick={(e) => mostrarPoliticas(e)}>
                                        <span>Políticas de garantía y devolución</span>
                                    </li>

                                    <li className={activarPagos === true ? "anchorFormas " : "anchorFormas"} style={(parseInt(tipo) !== 10) ? ver : ocultar} onClick={(e) => mostrarPagos(e)}>
                                        <span>Formas de Pago</span>
                                    </li>
                                </Fragment>
                                : <Fragment>
                                    <li className={(activarD === true) ? "laDescrip select" : "laDescrip "} onClick={(e) => mostrarDescripcion(e)}>
                                        <span>Descripción</span>
                                    </li>

                                    <li id="btnProdSucursales" className={(activarS === true) ? "prodSucursales select" : "prodSucursales"} style={(parseInt(tipo) === 10) ? ver : ocultar} onClick={(e) => mostrarSucursales(e)}>
                                        <span>Sucursales</span>
                                    </li>

                                    <li className={(activarP === true) ? "returnPolicy select" : "returnPolicy"} onClick={(e) => mostrarPoliticas(e)}>
                                        <span>Políticas de garantía y devolución</span>
                                    </li>

                                    <li className={activarPagos === true ? "anchorFormas select" : "anchorFormas"} style={(parseInt(tipo) !== 10) ? ver : ocultar} onClick={(e) => mostrarPagos(e)}>
                                        <span>Formas de Pago</span>
                                    </li>
                                </Fragment>
                            )
                        )
                    }
                </ul>

                <ul className="viewDescrip">
                    <DescripcionDescripcion activarD={activarD} general={general} atributos={atributos} />

                    {sucursal === true
                        ? <DescripcionSucursales activarS={activarS} tipo={tipo} disponibilidad={disponibilidad} style={(parseInt(tipo) === 10) ? verSucrusal : ocultar} />
                        : ""
                    }

                    <DescripcionPoliticas activarP={activarP} politicas={politicas} pagos={pagos} />

                    {pagos === true
                        ? <DescripcionPagos activarPagos={activarPagos} pagos={pagos} tipo={tipo} style={(parseInt(tipo) !== 10) ? ver : ocultar} />
                        : ""
                    }
                </ul>
            </div>
        );
    }
}