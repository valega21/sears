import React, { Component } from 'react'

const linea = {
    padding: '10px 0',
    borderBottom: '1px solid #dee2e6',
    display: "block"
}

const linea2 = {
    display: "block"
}

export default class DescripcionDescripcionComponent extends Component {

    render() {
        const { atributos, general, activarD } = this.props

        return (
            <li className={activarD === true ? 'laDescrip show' : 'laDescrip'}>
                {general.description}

                <div className="atributos">
                    {atributos.map((gene, i) => {
                        return (
                            <div key={i} style={Object.keys(atributos).length > 1 ? linea : linea2}>
                                <h5 className="h5Descripcion">
                                    {gene.title} :
                                </h5>
                                <span> {gene.value}</span>
                                <br />
                            </div>
                        );
                    })}
                </div>
            </li>
        );
    }
}