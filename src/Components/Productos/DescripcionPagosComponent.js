import React, { Component, Fragment } from 'react'

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'block'
}

export default class DescripcionPagosComponent extends Component {

    render() {
        const { pagos, tipo, activarPagos } = this.props

        return (
            <li className={activarPagos === true ? "anchorFormasCont show" : "anchorFormasCont"} style={(parseInt(tipo) !== 10) ? ver : ocultar}>

                {pagos.map((gene, i) => {
                    return (
                        (gene.action === "pay"
                            ? <Fragment key={i}>
                                {gene.content.map((ge, i) => {
                                    return (
                                        <div className="descformaspago" key={i}>
                                            <div className="descimg">
                                                <img alt={ge.text} src={ge.img}></img>
                                            </div>
                                            <div className="desctexto">
                                                {ge.text}
                                            </div>
                                        </div>
                                    );
                                })}

                            </Fragment>
                            : null
                        )
                    );
                })}
            </li>
        );
    }
}