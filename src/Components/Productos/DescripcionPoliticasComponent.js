import React, { Component, Fragment } from 'react'

export default class DescripcionPoliticasComponent extends Component {

    render() {
        const { pagos, politicas, activarP } = this.props

        return (

            <li id="returnPolicy" className={activarP ? 'returnPolicy show' : 'returnPolicy'} >
                {pagos.map((gene, i) => {
                    return (
                        (gene.action === "legacy"
                            ?
                            <Fragment key={i}>
                                {politicas.map((poli, i) => {
                                    return (
                                        <p key={i}>{poli.text}</p>
                                    );
                                })}
                            </Fragment> :
                            null
                        )
                    );
                })}
            </li>
        );
    }
}