import React, { Component, Fragment } from 'react'
import './DescripcionPromociones.sass'

const borde = {
    borderBottom: '1px solid #ddd'
}
const borde2 = {
    borderBottom: '1px solid #fff'
}
class DescripcionPromocionesL1 extends Component {
    constructor() {
        super();

        this.state = {
            active: false
        };
        this.toggleClass = this.toggleClass.bind(this);
    }

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    render() {
        const { item, collections, promociones, i } = this.props

        return (
            <dl className={this.state.active ? 'ofertasTarjetas cont-toggle viewPromo' : 'ofertasTarjetas  cont-toggle'} style={i > 0 ? borde : borde2} onClick={(e) => this.toggleClass(e)}>
                {promociones.isclick === true
                    ?
                    <dt>
                        {collections.map((col, i) => {
                            return (
                                (col.action === "promotion" ?
                                    <Fragment key={i}>
                                        {/* <i className="icoTarjeta" style={{ position: "absolute", left: "0px", top: "15px" }}>
                                            <img alt="tarjeta-promo" style={{ width: "80%" }} src={col.img}></img>
                                        </i> */}
                                        <p className="arrow" id="descripcion">{promociones.description}</p>
                                    </Fragment> :
                                    null
                                )
                            );
                        })}
                    </dt>
                    :
                    <dt style={{ cursor: "auto" }}>
                        <p className="" id="descripcion"> {promociones.description}</p>
                    </dt>
                }

                {item.map((ite, i) => {
                    return (
                        <dd className="dd-content" key={i}>
                            <p>{ite.text}</p>
                        </dd>
                    );
                })}
            </dl>
        );
    }
}

export default DescripcionPromocionesL1