import React, { Component, Fragment } from 'react'

class DescipcionPromocionesL2 extends Component {
    constructor() {
        super();

        this.state = {
            active: false
        };
        this.toggleClass = this.toggleClass.bind(this);
    }

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    render() {
        const { promociones, item } = this.props

        return (
            <Fragment>
                <dl className={this.state.active ? 'cont-toggle viewPromo' : 'cont-toggle'} onClick={(e) => this.toggleClass(e)}>
                    {promociones.isclick === true
                        ?
                        <dt style={{ fontWeight: "100" }}>
                            <p className="arrow" style={{}}>{promociones.description}</p>
                        </dt>
                        :
                        <dt style={{ cursor: "auto", fontWeight: "100" }}>
                            <p className="arrow" >{promociones.description}</p>
                        </dt>}

                    {item.map((ite, i) => {
                        return (
                            <dd className="dd-content" key={i}>
                                <p>{ite.text}</p>
                            </dd>
                        );
                    })}

                </dl>
            </Fragment>
        );
    }
}

export default DescipcionPromocionesL2