import React, { Component, Fragment } from 'react'

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'block',
    listStyle: "none"
}

export default class DescripcionSucursalesComponent extends Component {

    render() {
        const { tipo, disponibilidad, activarS} = this.props

        return (

            <li className={activarS === true ? 'ctnProdSucursales show' : 'ctnProdSucursales'} id="seccionSucursales" style={tipo === 10 ? ver : ocultar}>
                <div className="contSucursales">
                    <div className="titleContent">
                        <p className="titleTienda">Tiendas</p>
                        <p className="titleCant">Existencia</p>
                    </div>
                    {disponibilidad.map((tiendas, ii) => {
                        const tiend2 = Object.values(tiendas.stores)
                        return (
                            <Fragment key={ii}>
                                {tiend2.map((t, i) => {
                                    const tiend3 = Object.values(t.address)

                                    return (
                                        <Fragment key={i}>
                                            <dl>
                                                <dt><h5 className="h5sucursal">SEARS {tiendas.state} - {tiend3[5]} </h5> <span>{tiend3[1]}</span></dt>
                                            </dl>
                                            <p className="parrafoDireccion" key={i}><span><strong>Dirección:</strong></span> {tiend3[0]} {tiend3[2]} {tiend3[3]} {tiend3[5]} C.P. {tiend3[4]}</p>
                                        </Fragment>
                                    );
                                })
                                }
                            </Fragment>
                        );
                    })}
                </div>
            </li>
        );
    }
}