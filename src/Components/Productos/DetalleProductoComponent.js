import React, { Component, Fragment } from 'react'
import Botones from './BotonesComponent'
import Detalles from './DetallesComponent'
import Descripcion from './DescripcionComponent'
import SliderProducto from './SliderProductoComponent'
import SliderSimilares from './SliderSimilaresComponent'
import TallaColor from './TallaColorComponent'
import TiempoEntrega from './TiempoEntregaComponent'
import { Link } from 'react-router-dom'
import "chosen-js/chosen.css"
import "chosen-js/chosen.jquery.js"

import { connect } from 'react-redux'
import {
    showProductoPolo,
    showProducto,
    showProductoPagos,
    showPoliticaProducto,
    showProductoAtributo,
    showProductoSimilares,
    showProductoImagenes,
    showPromocionTarjeta,
    showPromocion,
    showCollections,
    showShippingFree,
    showMarca,
    showTipoProducto,
    showVendidoTienda,
    showDisponibilidad,
    color,
    tallaDefault
} from "../../Redux/Actions/DetalleProducto/productos"
import ModalClickRecoge from './ModalClickRecogeComponent'
import './DetalleProductoComponent.sass'

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'block'
}

class DetalleProductoComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nameProduct: "",
            tienda: "",
            price: "",
            colores: [],
            tallas: [],
            imgs: [],
            colorSeleccionado2: "",
            tallaSeleccionada2: "",
            skuSeleccionado: 0,
            isToggleOn: false,
            sucursal: false,
            descripcion: true,
            politicas: false,
            pagos: false,
            activarD: true,
            activarP: false,
            activarS: false,
            activarPagos: false,
            click: false,

            name: "",
            price: "",
            imgs: [],
            tall: ''
        }

        this.mostrarDescripcion = this.mostrarDescripcion.bind(this);
        this.mostrarSucursales = this.mostrarSucursales.bind(this);
        this.mostrarPoliticas = this.mostrarPoliticas.bind(this);
        this.mostrarPagos = this.mostrarPagos.bind(this);

        this.handleColor = this.handleColor.bind(this)
        this.handleTalla = this.handleTalla.bind(this)
    }

    mostrarDescripcion() {
        const currentState = this.state.descripcion;
        this.setState({ descripcion: !currentState })
        this.setState({ sucursal: false })
        this.setState({ politicas: false })
        this.setState({ pagos: false })

        const currentStateA = this.state.activarD;
        this.setState({ activarP: false })
        this.setState({ activarS: false })
        this.setState({ activarPagos: false })
        if (currentStateA === true) {
            this.setState({ activarD: currentStateA })
            this.setState({ isToggleOn: false })
        }
        else
            this.setState({ activarD: !currentStateA })
    }

    mostrarSucursales() {
        const currentState = this.state.sucursal;
        this.setState({ descripcion: false })
        this.setState({ politicas: false })
        this.setState({ pagos: false })

        if (currentState === true)
            this.setState({ sucursal: currentState })
        else
            this.setState({ sucursal: !currentState })

        const currentStateA = this.state.activarS;
        this.setState({ activarP: false })
        this.setState({ activarD: false })
        this.setState({ activarPagos: false })
        if (currentStateA === true) {
            this.setState({ activarS: currentStateA })

        }
        else
            this.setState({ activarS: !currentStateA })
    }

    mostrarPoliticas() {
        const currentState = this.state.politicas;
        this.setState({ descripcion: false })
        this.setState({ sucursal: false })
        this.setState({ politicas: !currentState })
        this.setState({ pagos: false })

        const currentStateA = this.state.activarP;
        this.setState({ activarS: false })
        this.setState({ activarD: false })
        this.setState({ activarPagos: false })
        if (currentStateA === true) {
            this.setState({ activarP: currentStateA })
            this.setState({ isToggleOn: false })
        }
        else
            this.setState({ activarP: !currentStateA })
    }

    mostrarPagos() {
        const currentState = this.state.pagos;
        this.setState({ descripcion: false })
        this.setState({ sucursal: false })
        this.setState({ politicas: false })

        if (currentState === true)
            this.setState({ pagos: currentState })
        else
            this.setState({ pagos: !currentState })

        const currentStateA = this.state.activarPagos;
        this.setState({ activarP: false })
        this.setState({ activarD: false })
        this.setState({ activarS: false })
        if (currentStateA === true) {
            this.setState({ activarPagos: currentStateA })
            this.setState({ isToggleOn: false })
        }
        else
            this.setState({ activarPagos: !currentStateA })
    }

    handleColor(event) {
        this.setState({ colorSeleccionado2: event })
        this.setState({ click: true })
        console.log("color seleccionado: ", this.state.colorSeleccionado2)
        // this.handleTalla()
    }

    handleTalla(value1) {
        this.setState({ tallaSeleccionada2: value1 })
        console.log("talla seleccionada: ", this.state.tallaSeleccionada2)
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.showProductoPolo(id);
        this.props.showProducto(id);
        this.props.showProductoPagos(id);
        this.props.showPoliticaProducto();
        this.props.showProductoAtributo(id);
        this.props.showProductoSimilares(id);
        this.props.showProductoImagenes(id);
        this.props.showPromocionTarjeta();
        this.props.showPromocion();
        this.props.showCollections(id);
        this.props.showShippingFree(id);
        this.props.showMarca(id);
        this.props.showTipoProducto(id);
        this.props.showVendidoTienda(id);
        this.props.showDisponibilidad(id);
        this.props.color(id);
        this.props.tallaDefault(id)
    }

    UNSAFE_componentWillReceiveProps(propi){
        if(propi.colorP!== this.state.colorSeleccionado2)
        this.setState({ colorSeleccionado2: propi.colorP })
    }

    componentDidUpdate() {
        const presionobotonSucursal = document.querySelector('.recogeEnTienda');
        window.scrollTo(0, 0)

        if (presionobotonSucursal) {
            presionobotonSucursal.addEventListener('click', this.handleClick);
            // window.scrollTo(1250,1250)
        }
    }

    handleClick = () => {
        const texto = document.querySelector('.mod__DescripcionP').getBoundingClientRect()
        this.setState({ isToggleOn: true })
        this.setState({ sucursal: true })
        this.setState({ activarD: false })
        this.setState({ activarP: false })
        window.scroll({ top: (texto.top + 30) })

        // {
        //     this.props.productos.map((prod, i) => {
        //         const talla2 = Object.values(prod.sizes)
        //         return (
        //             (this.state.colorSeleccionado2 === prod.color ?
        //                 // && i > 0 ?
        //                 (talla2.map((ta, it) => {

        //                     return (
        //                         (this.state.tallaSeleccionada2 === ta.size
        //                             ?
        //                             this.setState({ skuSeleccionado: ta.sku })
        //                             : null
        //                         )
        //                     );
        //                 }
        //                 ))
        //                 : null
        //                 // (talla2.map((ta, it) => {

        //                 //     return (
        //                 //         (this.state.tallaSeleccionada2 === ta.size
        //                 //             ?
        //                 //             this.setState({ skuSeleccionado: ta.sku })
        //                 //             : null
        //                 //         )
        //                 //     );
        //                 // }
        //                 // ))
        //             )
        //         )
        //     })
        // }
        // console.log("sku seleccionado: ", this.state.skuSeleccionado)
    }

    render() {
        const {
            atributos,
            collections,
            general,
            imagenes,
            marca,
            pagos,
            politicas,
            productos,
            promociones,
            promoGeneral,
            shipping,
            similares,
            tipo,
            tienda,
            disponibilidad,
            colorP,
            tallaP
        } = this.props

        return (
            <div className="productMainContainer">
                <div className="mod__producto">
                    {/* Slider producto */}
                    <div className="viewsNdescrip">
                        <div className="info-Product">

                            <SliderProducto imagenes={imagenes} general={general} />

                            <div className={parseInt(tipo) !== 10 ? "buy-Details" : "buy-Details contenedorTienda"}>
                                <Detalles
                                    general={general}
                                    pagos={pagos}
                                    promociones={promociones}
                                    promoGeneral={promoGeneral}
                                    collections={collections}
                                    shipping={shipping}
                                    marca={marca}
                                    tipo={tipo}
                                    tienda={tienda}
                                />

                                <div className="opcionesPrincipal" style={(parseInt(tipo) === 10) ? ver : ocultar}>
                                    {/* Selectores de cantidad, color y tallas */}
                                    <TallaColor
                                        productos={productos}
                                        general={general}
                                        handleColor={this.handleColor}
                                        handleTalla={this.handleTalla}
                                        tipo={tipo}
                                        colorP={colorP}
                                        tallaP={tallaP}
                                    />
                                </div>

                                {/* <SeccionenTiendas tipo={tipo} disponibilidad={disponibilidad} handleClick={this.handleClick} actiSucursal={actiSucursal}/> */}

                                <Fragment>
                                    {tipo === 10
                                        ?
                                        <Fragment>
                                            <div className="purchase_btns">
                                                <form>
                                                    <div className="fieldset">
                                                        {/* <button type="button" className="recogeEnTienda" onClick={handleClick}>Buscar Producto en Tienda Sears</button> */}
                                                        <a  href="#seccionSucursales"><button type="button"  className="recogeEnTienda" >Buscar Producto en Tienda Sears</button></a>
                                                    </div>
                                                </form>
                                            </div>

                                            <div className="tarjetaSears">
                                                <div className="cont_tarS">
                                                    <p>Aprovecha los mejores descuentos y promociones utilizando tu Tarjeta Sears!</p>
                                                    <p className="min-space-top">Si no la tienes, <Link className="a" to="/credito-sears/requisitos-y-formato-de-solicitud" target="_blank">Solicítala AQUI</Link></p>
                                                </div>
                                            </div>
                                        </Fragment>
                                        :
                                        null
                                    }
                                </Fragment>
                            </div>
                        </div>

                    </div>

                    <div className={parseInt(tipo) !== 10 ? "purchaseModule" : ""} style={parseInt(tipo) !== 10 ? ver : ocultar}>
                        {/* Selectores de cantidad, color y tallas */}
                        <form name="det_compra" id="det_compra">
                            <TallaColor
                                productos={productos}
                                general={general}
                                handleColor={this.handleColor}
                                handleTalla={this.handleTalla}
                                tipo={tipo}
                                colorP={colorP}
                                tallaP={tallaP}
                            />
                        </form>

                        {/* Grupo de Botones Comprar ahora, Agregar a bolsa, Recoger en tienda */}
                        <div className="purchase_btns">
                            {/* <Botones
                                colores={this.obtenerColor}
                                nameProduct={general.title}
                                price={general.price}
                                imgs={imagenes}
                                productos={productos}
                                colorSeleccionado2={this.state.colorSeleccionado2}
                                tallaSeleccionada2={this.state.tallaSeleccionada2}
                                tipo={tipo}
                                general={general}
                            /> */}

                            <form style={parseInt(tipo) !== 10 ? ver : ocultar} >
                                <div className="fieldset">
                                    <button id="comprar" className={general.stock === 0 ? " disabledComprar" : "btn-compra2"} type="submit" disabled={parseInt(general.stock) === 0 ? true : false}>Comprar Ahora</button>
                                    <button id="addCarrito" href="#!" className={general.stock === 0 ? "disabledBolsa" : "btn-carrito slidcar"} type="button" disabled={parseInt(general.stock) === 0 ? true : false}>Agregar a bolsa</button>

                                    <ModalClickRecoge
                                        name={this.state.nameProduct} price={this.state.price} imgs={this.state.imgs} productos={productos}
                                        colorSeleccionado2={this.state.colorSeleccionado2} 
                                        tallaSeleccionada2={this.state.tallaSeleccionada2} 
                                        general={general} colorP={colorP}
                                    />
                                </div>

                                <div className="mensajeAgotado" id="mensajeAgotado" style={general.stock === 0 ? ver : ocultar}>
                                    <div className="alertaAgotado">
                                        <span>Producto Agotado</span>
                                    </div>
                                </div>
                            </form>
                        </div>

                        {/* seccion calcular tiempo de entrega con codigo de area */}
                        <div className="box_claroEnvios" style={((parseInt(tipo) !== 10) && (general.stock !== 0)) ? ver : ocultar}>
                            <TiempoEntrega collections={collections} />
                        </div>
                    </div>

                    {/* Seccion Productos Similares */}
                    <div className="slidersDetalleProducto">
                        <SliderSimilares similares={similares} general={general} />
                    </div>

                    {/* seccion descripcion */}
                    <div className="mod__DescripcionP" id="seccionSucursales">
                        <Descripcion general={general} pagos={pagos} politicas={politicas} atributos={atributos}
                            tipo={tipo} disponibilidad={disponibilidad} isToggleOn={this.state.isToggleOn}
                            activarS={this.state.activarS} activarD={this.state.activarD} activarP={this.state.activarP} activarPagos={this.state.activarPagos}
                            sucursal={this.state.sucursal}
                            mostrarDescripcion={this.mostrarDescripcion} mostrarPagos={this.mostrarPagos} mostrarPoliticas={this.mostrarPoliticas} mostrarSucursales={this.mostrarSucursales}
                        />
                    </div>
                </div>
            </div >
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        productos: state.producto.list,
        general: state.producto.list2,
        pagos: state.producto.list3,
        politicas: state.producto.list4,
        atributos: state.producto.list5,
        similares: state.producto.list6,
        imagenes: state.producto.list7,
        promociones: state.producto.list8,
        promoGeneral: state.producto.list9,
        collections: state.producto.list10,
        shipping: state.producto.list11,
        marca: state.producto.list12,
        tipo: state.producto.list13,
        tienda: state.producto.list14,
        disponibilidad: state.producto.list15,
        colorP: state.producto.list16,
        tallaP: state.producto.list17
    }
}

export default connect(mapStateToProps, {
    showProductoPolo,
    showProducto,
    showProductoPagos,
    showPoliticaProducto,
    showProductoAtributo,
    showProductoSimilares,
    showProductoImagenes,
    showPromocionTarjeta,
    showPromocion,
    showCollections,
    showShippingFree,
    showMarca,
    showTipoProducto,
    showVendidoTienda,
    showDisponibilidad,
    color,
    tallaDefault
})(DetalleProductoComponent)