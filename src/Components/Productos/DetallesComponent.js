import React, { Component, Fragment } from 'react'
import Dl from './DetallesDlComponent'
import Dl2 from './DetallesDl-2nivelComponent'
import DescripcionPromoL2 from './DescripcionPromocionesL2'
import DescripcionPromoL1 from './DescripcionPromocionesL1'

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'initial'
}

class DetallesComponent extends Component {
    render() {
        const { collections, general, marca, promociones, promoGeneral, shipping, tipo, tienda } = this.props

        return (
            <Fragment>
                <div className="productMainContainer">
                    <h1>{general.title}</h1>
                </div>

                <div className="skuMarca">

                    <div className={(tipo === 3 || tipo === 2) ? "left" : "divtienda"}>
                        <h3 style={tipo === 3 ? ver : ocultar}>Marca <span> {marca.marca}</span></h3>
                        <h3 className="tituloTienda" style={tipo !== 3 ? ver : ocultar}>SKU#: <span> {general.sku}</span></h3>
                    </div>

                    <div className={(tipo === 3 || tipo === 2) ? "right" : "divtienda"}>
                        <h3 style={tipo === 3 ? ver : ocultar}>SKU <span>{general.sku}</span></h3>
                        <h3 className="tituloTienda" style={tipo !== 3 ? ver : ocultar}>Marca: <span> {marca.marca}</span></h3>
                    </div>

                </div>

                <div className="price">
                    <p className="antes" style={(tipo === 3 || tipo === 2) ? ver : ocultar}>
                        <span className="priceList">${general.sale_price}</span>
                    </p>

                    {(tipo === 3 || tipo === 2)
                        ?
                        <p className="total" id="precioCambio" >
                            <span>${general.price}</span>
                            <span className="span"> MXN</span>
                        </p>
                        :
                        <p className="total" id="precioCambio">
                            <span><h5 style={{ display: "inline" }}>Precio Tienda: </h5> ${general.price}</span>
                            <span className="span"> MXN</span>
                        </p>
                    }
                </div>

                {shipping.isshipping === true
                    ?
                    <div className="envioGratis">
                        <p>Envío gratis</p>
                    </div>
                    : null
                }

                <div className="mejorOferta" style={(tipo === 3 || tipo === 2) ? ver : ocultar}>
                    <div className="ofertasSears">
                        <div className="cont-promo">
                            {promoGeneral.map((promoG, i) => {
                                const item = Object.values(promoG.promotion)

                                return (
                                    (i === 0
                                        ? <Dl key={i} promoG={promoG} item={item} collections={collections}/>

                                        : <Dl2 key={i} promoG={promoG} item={item}/>
                                    )
                                );
                            })}
                        </div>
                    </div>
                    <Fragment>
                        {/* <div className="ofertasTarjetas"> */}
                        {/* {pagos.map((pa) => { */}
                        {/* return (
                                ((pa.id === 2) && (pa.action == "promotion")
                                    ? <Fragment key={pa.id}> */}
                        {promociones.map((promo, i) => {
                            const item = Object.values(promo.promotion)
                            return (
                                <Fragment key={i}>
                                    {i === 0
                                        ? <DescripcionPromoL1 promociones={promo} i={i} item={item} collections={collections} />
                                        : <DescripcionPromoL2 promociones={promo} item={item} />
                                    }
                                </Fragment>
                            );
                        })}
                        {/* </Fragment>
                                    : null)
                            ); */}
                        {/* })} */}
                        {/* </div> */}
                    </Fragment>
                </div>

                { tipo === 10
                    ?
                    <div className="textoEnTienda">
                        {tienda.map((tien, i) => {
                            return (
                                (i === 0
                                    ?
                                    <h3 key={i}> {tien.text}</h3>
                                    :
                                    <p key={i}>{tien.text}</p>
                                )
                            );
                        }
                        )}
                    </div>
                    :
                    null
                }
            </Fragment>
        );
    }
}

export default DetallesComponent