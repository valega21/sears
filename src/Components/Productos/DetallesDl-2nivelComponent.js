import React, { Component } from 'react';

class DetallesDl2Component extends Component {
    
    constructor() {
        super();

        this.state = {
            active: false
        };
        this.toggleClass = this.toggleClass.bind(this);
    }

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    render() {
        const { promoG, item } = this.props

        return (
            <dl className={this.state.active ? 'cont-toggle viewPromo' : "cont-toggle"} onClick={(e) => this.toggleClass(e)}>
                {promoG.isclick === true
                    ? <dt>
                        <p className="arrow">{promoG.description}</p>
                    </dt>
                    : <dt style={{ cursor: "auto" }}>
                        <p className="arrow" >{promoG.description}</p>
                    </dt>}

                {item.map((ite, i) => {
                    return (
                        <dd className="dd-content" key={i}>
                            <p>{ite.text}</p>
                        </dd>
                    );
                })}

            </dl>
        );
    }
}

export default DetallesDl2Component;