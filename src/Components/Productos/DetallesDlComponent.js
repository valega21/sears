import React, { Component, Fragment } from 'react'

class DetallesDlComponent extends Component {
    constructor() {
        super();

        this.state = {
            active: false
        };
        this.toggleClass = this.toggleClass.bind(this);
    }

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    render() {
        const { item, collections, promoG } = this.props

        return (
            <dl className={this.state.active ? 'tarjetaSears  cont-toggle viewPromo' : 'tarjetaSears  cont-toggle'} onClick={(e) => this.toggleClass(e)}>
                {promoG.isclick === true
                    ? <dt>
                        {collections.map((col, i) => {
                            return (
                                (col.action === "promotions_sears" ?
                                    <Fragment key={i}>
                                        <i className="icoTarjeta" style={{ position: "absolute", left: "0px", top: "15px" }}>
                                            <img alt="tarjeta-sears" style={{ width: "80%" }} src={col.img}></img>
                                        </i>
                                        <p className="arrow">{promoG.description}</p>
                                    </Fragment> :
                                    null
                                )
                            );
                        })}
                    </dt>
                    : <dt style={{ cursor: "auto" }}>
                        <p className="arrow" > {promoG.description}</p>
                    </dt>
                }

                {item.map((ite, i) => {
                    return (
                        <dd className="dd-content" key={i}>
                            <p>{ite.text}</p>
                        </dd>
                    );
                })}
            </dl>
        );
    }
}

export default DetallesDlComponent