import React, { Component } from "react"
import $ from 'jquery'

export default class LiComponent extends Component {

    constructor() {
        super();
        this.state = {
            active: false
            // urlimagenLi: ''
        };

        this.toggleClass = this.toggleClass.bind(this);
        this.imagenSeleccionada = this.imagenSeleccionada.bind(this);
        this.envioPadre = this.envioPadre.bind(this);
    }

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });

        if ($("li ").hasClass('active')) {
            $("li ").removeClass('active')
        };
    };

    imagenSeleccionada(e) {
        this.setState({ urlimagenLi: e.target.src });
    };

    envioPadre(e) {
        e.preventDefault();
        const dataIndex1 = e.target.getAttribute('data-slide-index');

        var indiceinput = document.getElementById('input1').value = dataIndex1;
        console.log("", indiceinput)
        this.props.indice(dataIndex1)
    }

    render() {
        const { ima, imaActual, ind } = this.props

        return (
            (imaActual === ima.link ?
                <li className='active' onClick={(e) => this.toggleClass(e)} key={ima.id} aria-hidden="false">
                    <img id={ima.id} data-slide-index={ind} onClick={(e) => { this.props.seleccion1(ima.link); this.envioPadre(e); }} alt="imagenes-producto" src={ima.link} ></img>
                </li>
                :
                <li className={this.state.active ? 'active' : ''} onClick={(e) => this.toggleClass(e)} key={ima.id} aria-hidden="false">
                    <img id={ima.id} data-slide-index={ind} onClick={(e) => { this.props.seleccion1(ima.link); this.envioPadre(e); }} alt="imagenes-producto" src={ima.link} ></img>
                </li>
            )
        );
    }
}