import React, { PureComponent, Fragment } from 'react'
import Modal from 'react-modal'
import './ModalclickRecogeComponent.sass'
import { data } from './tiendas.json'
import Chosen from './BuscadorComponent'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { cambiarTalla } from '../../Redux/Actions/DetalleProducto/modal.js'
import { cambiarColor, colorSeleccionado } from '../../Redux/Actions/DetalleProducto/actionModalColor.js'

const customStyles = {
    content: {
        top: '20%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        width: '417px',
        height: "600px",
        transform: 'translate(-50%, -20%)',
        overflow: 'initial'
    }
}

class ModalclickRecogeComponent extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            hover: false, // bandera abrir o cerrar modal
            modalIsOpen: false, // bandera modal abierto
            activo: false,
            data,
            event: 0,
            colorSeleccionadoRecibido: "", // color seleccionado del producto recibido en el modal
            tallaSeleccionadaRecibida: "" // talla seleccionada en el producto recibida en el modal
        };

        this.hoverOn = this.hoverOn.bind(this);
        this.hoverOff = this.hoverOff.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeTalla = this.handleChangeTalla.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    hoverOn() {
        this.setState({ hover: !this.state.hover });
    }

    hoverOff() {
        this.setState({ hover: false });
    }

    /** tomar valor del select */
    handleChange(evento) {
        this.setState({ event: evento });
    }

    /** tomar valor del select de color */
    handleChangeColor(evento) {
        this.setState({ colorSeleccionadoRecibido: evento.target.value });
        this.props.cambiarTalla(this.state.tallaSeleccionadaRecibida)
        this.props.colorSeleccionado(evento.target.value)
    }

    /** tomar valor del select de talla */
    handleChangeTalla(event) {
        this.setState({ tallaSeleccionadaRecibida: event.target.value });
        this.props.cambiarTalla(event.target.value)
        this.props.handleTalla(event.target.value)
    }

    /** abrir modal */
    openModal() {
        this.setState({ modalIsOpen: true });

        if (this.props.colorSelec == "") {
            this.setState({ colorSeleccionadoRecibido: this.props.colorSeleccionado2 });
        } else {
            this.setState({ colorSeleccionadoRecibido: this.props.colorSelec })
        }
    }

    /** aplicar propiedades al abrir modal */
    afterOpenModal() {
        this.subtitle.style.color = '#000';
    }

    /** cerrar modal */
    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    UNSAFE_componentWillReceiveProps(next) {
        this.setState({ colorSeleccionadoRecibido: next.colorSelec })
        this.setState({ tallaSeleccionadaRecibida: next.tallaSeleccionada2 })
    }

    render() {
        const { name, price, imgs, productos, general } = this.props
        const { data } = this.state;
        // console.log("vaciO: ", this.props.colorSelec)
        return (
            <Fragment>
                <button id="btnClickRecoge" onClick={this.openModal} className={parseInt(general.stock) === 0 ? "disabledClick" : "btn-click"} type="button" disabled={parseInt(general.stock) === 0 ? true : false}>Recoger en Tienda</button>

                <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} styles={customStyles} contentLabel="Click y Recoge" ariaHideApp={false} >

                    <div className="contenedorModal">
                        <button title="close" onClick={this.closeModal} className="fancybox-item fancybox-close"> <span>&times;</span></button>
                        <div className="ico-click-recoge bordeBottom">
                            <h2 ref={subtitle => this.subtitle = subtitle} className="clickRecoge">Click y recoge</h2>
                        </div>

                        <article className="Box__product">
                            <div className="fila_producto">
                                <div className="box__detalleClick">
                                    {imgs.map((img, i) => {
                                        return (
                                            (img.order === 1
                                                ? <img alt="imagen principal producto" key={i} src={img.link} className="tienda_Detalle"></img>
                                                : null
                                            )
                                        );
                                    })}
                                </div>

                                <div className="caja__click">
                                    <p className="producto descProducto">{name}</p>
                                    <div className="info">
                                        <p>Vendido por: </p>
                                        <p className="cajaClick">${price}</p>
                                    </div>
                                </div>

                                <div className="opciones__Talla">
                                    <div className="boxColor__Talla ">
                                        <span>Color</span>
                                        <div className="opc_colors">
                                            <select name="tallaCR" id="colorCR" defaultValue={this.state.colorSeleccionadoRecibido} onChange={this.handleChangeColor}>
                                                {productos.map((prod, i) => {
                                                    return (
                                                        <option key={i} value={prod.color}>{prod.color}</option>
                                                    );
                                                })}
                                            </select>
                                        </div>
                                    </div>

                                    <div className="boxTallas__Talla ">
                                        <span>Tallas</span>
                                        <div className="opc_colors">
                                            <select name="tallaCR" id="tallaCR" defaultValue={this.state.tallaSeleccionadaRecibida} >
                                                {productos.map((tal) => {
                                                    const talla2 = Object.values(tal.sizes)
                                                    return (talla2.map((ta, it) => {
                                                        return (
                                                            (this.state.colorSeleccionadoRecibido === tal.color ?
                                                                <option key={it} value={ta.size} > {ta.size}</option>
                                                                : null
                                                            )
                                                        );
                                                    }
                                                    ));
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <div className="content__Padres">
                            <div className="cont-search-tienda">
                                <div>
                                    <p> Selecciona tu tienda por:</p>
                                </div>

                                <div className="display-inline2">
                                    <section>
                                        <div className="search display-inline">
                                            <div style={{ display: "none" }}>
                                                <a id="lnkMuestraSucursales" href="#!" className="link">Ver tiendas</a>
                                            </div>
                                        </div>
                                    </section>
                                </div>

                                <div id="pnlTiendas" className="tiendasSearch display-inline2">
                                    <section>
                                        <p id="txtNombre" className="conttxt">Nombre de Sucursal</p>
                                        <Chosen className="chosen-container chosen-container-single" data={data} event={this.handleChange} />
                                    </section>
                                </div>
                            </div>

                            <div className="padre1" id="contenedorTiendasCR">
                                {data.map((tiendas, i) => {
                                    return (
                                        <Fragment key={i}>
                                            {parseInt(this.state.event) === tiendas.id
                                                ?
                                                <article className="box__distancia" id="artSucursal6" key={tiendas.id}>
                                                    <div className="contInfo izqFlotante">
                                                        <ul>
                                                            <li className="titTienda">{tiendas.tienda}</li>
                                                        </ul>
                                                    </div>
                                                    <p className="descTienda">{tiendas.direccion}</p>
                                                    <p className="Horarios__Tienda">{tiendas.horario}</p>
                                                    <p className="telTienda">{tiendas.telefono}</p>
                                                    <a id="lnkClickRecoge" href="/login" className="Tienda">Recoge Aquí</a>
                                                </article>
                                                :
                                                null
                                            }
                                            {parseInt(this.state.event) === 0 || this.state.event === "-1"
                                                ?
                                                <article className="box__distancia" id="artSucursal6" key={tiendas.id}>
                                                    <div className="contInfo izqFlotante">
                                                        <ul>
                                                            <li className="titTienda">{tiendas.tienda}</li>
                                                        </ul>
                                                    </div>
                                                    <p className="descTienda">{tiendas.direccion}</p>
                                                    <p className="Horarios__Tienda">{tiendas.horario}</p>
                                                    <p className="telTienda">{tiendas.telefono}</p>
                                                    <Link id="lnkClickRecoge" to="/login" className="Tienda">Recoge Aquí</Link>
                                                </article>
                                                :
                                                null
                                            }
                                        </Fragment>
                                    );
                                }
                                )}
                            </div>
                        </div>
                    </div>
                </Modal>
            </Fragment >
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        cambiarT: state.cambiarT,
        cambiarC: state.cambiarC,
        colorSelec: state.colorS.cambiarColor
    }
}

export default connect(mapStateToProps, {
    cambiarTalla, cambiarColor, colorSeleccionado
})(ModalclickRecogeComponent)