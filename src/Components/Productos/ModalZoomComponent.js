import React, { Component } from 'react'
import Modal from 'react-modal'
import Li from './LiComponent.js'
import './ModalZoomComponent.sass'

//estilos aplicados al modal
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        width: '90vw',
        height: "100vh",
        transform: 'translate(-50%, -50%)',
        maxWidth: '1400px',
        maxHeight: '800px',
        backgroundColor: 'white',
        overflow: 'hidden'
    }
}

export default class ModalZoomComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false, //bandera para abrir el modal
            claseZoom: false, // bandera para aplicar zoom
            seleccion: '',
            click: false,
            indice: 0
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.zoom = this.zoom.bind(this);
        this.seleccionImagen = this.seleccionImagen.bind(this);
        this.indice = this.indice.bind(this);
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        this.subtitle.style.color = '#000';
    }

    zoom() {
        const currentState = this.state.claseZoom;
        this.setState({ claseZoom: !currentState });
    }

    seleccionImagen(sel) {
        this.setState({ seleccion: sel })

        if (sel !== "")
            this.setState({ click: true })
    }

    indice(ind) {
        this.setState({ indice: ind })
        this.props.recibir(this.state.indice)
    }

    componentDidMount() {
        this.props.cerrar(this.state.modalIsOpen)
    }

    render() {
        const { general, open, imagenes, img, cerrar, imaActual } = this.props

        return (
            <Modal isOpen={open} onRequestClose={cerrar} style={customStyles} aria-modal="true" ariaHideApp={false}>
                <div className="bodyZoom">
                    <div className="contZoomRelative">
                        <button title="close" onClick={cerrar} className="closeZoom"> <span className="closeDes">&times;</span></button>

                        <div className="contentZoom" onClick={this.zoom}>
                            {this.state.click === true
                                ? <img alt="imagen-producto" className={this.state.claseZoom ? 'zoomin hover' : 'zoomin'} onClick={(e) => this.zoom(e)}
                                    src={this.state.seleccion} style={{ height: '450px' }}></img>
                                :
                                (this.state.click === true && this.state.seleccion === "" //condicion para visualizar el cambio de la imagen principal en el modal  (no funciona bien)
                                    ?
                                    <img alt="imagen-producto" className={this.state.claseZoom ? 'zoomin hover' : 'zoomin'} onClick={(e) => this.zoom(e)}
                                        src={img} style={{ height: '450px' }}></img>
                                    :
                                    <img alt="imagen-producto" className={this.state.claseZoom ? 'zoomin hover' : 'zoomin'} onClick={(e) => this.zoom(e)}
                                        src={img} style={{ height: '450px' }}></img>

                                )
                            }

                        </div>

                        <div className="navigateZoom">
                            <h2 style={{ color: "#000" }}>{general.title}</h2>
                            <ul>
                                {imagenes.map((ima, id) => {
                                    return (
                                        <Li imaActual={imaActual} indice={this.indice} ind={id} key={id} ima={ima} seleccion1={this.seleccionImagen} />
                                    );
                                })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}