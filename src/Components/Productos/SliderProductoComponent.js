import React, { Component } from 'react'
import Slider from 'react-slick'
// import 'slick-carousel/slick/slick.css'
// import 'slick-carousel/slick/slick-theme.css'
import ModalZoom from './ModalZoomComponent'


export default class SliderProductoComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nav1: null,
            nav2: null,
            modalIsOpen: false,
            imaActual: '',
            slideIndex: 0,
            updateCount: 0,
            indice: 0,
            inputPrueba: 0
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.recibir = this.recibir.bind(this);
    }

    componentDidMount() {
        this.setState({
            nav1: this.slider1,
            nav2: this.slider2,
        });

    }

    openModal(ima) {
        this.setState({ modalIsOpen: true });
        this.imagenActual(ima)
        this.setState({ imaActual: ima });
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    imagenActual(event) {
        //this.setState({ imaActual: event.target.src });
        this.setState({ imaActual: event });
    }

    cambiarImagen(event) {
        this.setState({ imaActual: event.target.src });
    }

    enviar = (e) => {
        e.preventDefault();
        let dataIndex = e.target.getAttribute('data-slide-index');
        this.slider.slickGoTo(dataIndex);
    }

    recibir(item) {
        this.setState({ indice: item })

        var indiceinput = document.getElementById('input1').value;
        this.slider.slickGoTo(indiceinput);
    }

    render() {
        // console.log("imagen actual:", this.state.imaActual)
        const { imagenes, general } = this.props
        const settings = {
            afterChange: () =>
                this.setState(state => ({ updateCount: state.updateCount + 1 })),
            beforeChange: (next) => this.setState({ slideIndex: next }),
            responsive: [
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 7,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                    }
                }
            ]
        }

        const config = {
            responsive: [
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 7,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                    }
                }
            ]
        }
        return (
            <div className="media-container" >
                <div className="bx-wrapper slider-for">
                    <div className=" bx-viewport">
                        <ul className="carrusel-producto slider-single">
                            <Slider
                                // asNavFor={this.state.nav2} ref={slider => (this.slider1 = slider)}  {...settings} 
                                ref={slider => (this.slider = slider)}
                                {...config}
                            >
                                {imagenes.map((ima, i) => {
                                    return (
                                        <li key={ima.id} aria-hidden="false" className="item">
                                            <img id={ima.id} onClick={(e) => this.openModal(ima.link)} alt="imagenes-producto" src={ima.link}></img>
                                            <ModalZoom
                                                general={general}
                                                imagenes={imagenes}
                                                open={this.state.modalIsOpen}
                                                img={this.state.imaActual}
                                                cerrar={this.closeModal}
                                                imaActual={this.state.imaActual}
                                                recibir={this.recibir}
                                            >
                                            </ModalZoom>
                                        </li>
                                    );
                                })
                                }
                            </Slider>
                        </ul>
                    </div>
                </div>

                <Slider
                    // asNavFor={this.state.nav1}
                    //  ref={slider => (this.slider2 = slider)}
                    // swipeToSlide={true}
                    focusOnSelect={true} //cambiar al presionar imagen slider inferior
                    slidesToScroll={1} infinite={false} slidesToShow={7} initialSlide={0}
                    {...settings}
                >

                    {imagenes.map((ima, ind) => {
                        return (
                            <a href="#!" onClick={this.enviar} className="outborder active " key={ima.id} style={{
                                float: 'left',
                                listStyle: 'none',
                                position: 'relative',
                                width: '200px'
                            }}>
                                <img data-slide-index={ind} alt="imagenes-producto" src={ima.link}></img>
                            </a>
                        );
                    })}

                </Slider>
                <div className="bx-controls bx-has-controls-direction">
                    <div className="bx-controls-direction ">
                        <div className=""><input type="text" id="input1" defaultValue="" style={{ visibility: 'hidden' }}></input></div>
                        <button className=" slick-arrow slick-prev bx-prev disabled " id="preview" style={{ visibility: 'hidden' }}>prev</button>
                        <button className="slick-arrow slick-next bx-next " id="next" style={{ visibility: 'hidden' }}>next</button>
                    </div>
                </div>
            </div>
        );
    }
}