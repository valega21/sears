import React, { Component } from 'react';
import Slider from 'react-slick';

class SliderSimilaresComponent extends Component {
    render() {
        const { similares } = this.props

        var settings = {
            infinite: true,
            speed: 600,
            slidesToShow: 4,
            //slidesToScroll: 4,
            //adaptiveHeight: true,
            autoplay: true,
            autoplaySpeed: 5000,
            initialSlide: 0,
            centerMode: false,
            draggable: false,
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 360,
                    settings: { slidesToShow: 1, slidesToScroll: 1, initialSlide: 0, adaptiveHeight: true, infinite: true }
                },
                {
                    breakpoint: 387,
                    settings: { slidesToShow: 1.17, slidesToScroll: 1, initialSlide: 0, adaptiveHeight: true, infinite: true }
                },
                {
                    breakpoint: 564,
                    settings: { slidesToShow: 1.40, slidesToScroll: 2, initialSlide: 0, adaptiveHeight: true, infinite: true }
                },
                {
                    breakpoint: 695,
                    settings: { slidesToShow: 2, slidesToScroll: 2, initialSlide: 0, adaptiveHeight: true, infinite: true }
                },
                {
                    breakpoint: 861,
                    settings: { slidesToShow: 2.55, slidesToScroll: 2, initialSlide: 0, adaptiveHeight: true, infinite: true }
                },

                {
                    breakpoint: 1024,
                    settings: { slidesToShow: 3, slidesToScroll: 3, initialSlide: 0, adaptiveHeight: true, infinite: true }
                }
            ]
        }

        return (
            <div className="productosdetalle Click">
                <h2>Productos Similares</h2>

                <div id="rel1" className="rowCuadrosProdDetalle slick-initialized slick-slider">
                    {/* <div className="slick-list draggable"> */}
                    {/* <div className="slick-track"> */}
                    <button className="slick-prev slick-arrow" type="button">Prev</button>
                    <Slider {...settings}>

                        {similares.map((simi, i) => {
                            return (

                                <a className="cuadroProdDetalle slick-slide slick-cloned" href="#!" key={i}>
                                    <div className="cuadroImgDetalle contImgProd">
                                        <img alt="similares" src={simi.img}></img>
                                        <span>{simi.title}</span>
                                    </div>
                                </a>

                            );
                        })}
                        <button className="slick-next slick-arrow" type="button">Next</button>
                    </Slider>
                    {/* </div> */}
                    {/* </div> */}
                </div>
            </div>
        );
    }
}
export default SliderSimilaresComponent;
