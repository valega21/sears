import React, { Component, Fragment } from 'react'
import $ from 'jquery'
import { connect } from 'react-redux'
import { cambiarTalla } from '../../Redux/Actions/DetalleProducto/modal.js'
import { cambiarColor, colorSeleccionado } from '../../Redux/Actions/DetalleProducto/actionModalColor.js'
// import { skuDefault, skuSeleccionado } from "../../Redux/Actions/DetalleProducto/productos"

const ocultar = {
    display: 'none'
}

const ver = {
    display: 'initial'
}

class TallaColorComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            colorSeleccionadoSelect: "",// color seleccionado en el select
            tallaDefault: this.props.tallaP,
            tallaDefaultProp: "", // talla por defecto recibida en la prop
            // skuSeleccionado: "",
            // skuPorDefecto: ""
        };

        this.handleChangeColor = this.handleChangeColor.bind(this)
        this.handleChangeTalla = this.handleChangeTalla.bind(this)
        this.cambiarTallasSegunColor = this.cambiarTallasSegunColor.bind(this)
        // this.skuSeleccionado = this.skuSeleccionado.bind(this)
    }

    handleChangeColor(event) {
        this.setState({ tallaDefaultProp: event.target.value })
        this.props.cambiarColor(event.target.value);
        this.props.handleColor(this.state.colorSeleccionadoSelect);
        this.props.colorSeleccionado(event.target.value);
        this.props.cambiarTalla(this.state.tallaDefault)
        this.setState({ colorSeleccionadoSelect: event.target.value });
    }

    handleChangeTalla(event) {
        this.setState({ tallaDefault: event.target.value });
        this.props.cambiarTalla(event.target.value)
        this.props.handleTalla(event.target.value)
    }

    cambiarTallasSegunColor(event) {
        this.setState({ colorSeleccionadoSelect: event.target.value });
    }

    componentDidMount() {
        $('.countValue').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    }

    UNSAFE_componentWillReceiveProps(next) {
        this.setState({ colorSeleccionadoSelect: next.colorP })
        this.setState({ tallaDefaultProp: next.tallaP })
        // this.setState({ skuPorDefecto: next.skuD })
        // this.setState({ skuSeleccionado: next.skuD })
    }

    // skuSeleccionado(e) {
    //     // this.setState({ skuSeleccionado: e.target.value })
    //     // this.props.skuSeleccionado(e.target.value)
    //     // console.log("target name: ", this.state.skuSeleccionado)
    // }

    render() {
        const { productos, general, tipo } = this.props

        {/* <input id="sku" value={ta.sku} style={{ visibility: "hidden" }} /> */ }
        // console.log("sku: ", this.state.skuPorDefecto)
        return (
            <Fragment>
                <div className="cantidadOpt" style={((parseInt(tipo) === 3 && (general.stock !== 0)) || (parseInt(tipo) === 2 && (general.stock !== 0))) ? ver : ocultar}>
                    <div className="moduloCompra">
                        <div className="text-cantidad">
                            <h3>Cantidad</h3>
                            <div className="fieldset">
                                <div className="countBox">
                                    <input className="countValue" type="text" id="cantidad" name="cantidad" min="1" max={general.stock} defaultValue="1"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* COLOR */}
                {Object.keys(productos).length !== 0
                    ? <div className="opciones">
                        <div className="boxColor">
                            <span>Color</span>
                            <div className="opc_colors">
                                <select name="color" id="color" defaultValue={this.state.colorSeleccionadoSelect} onChange={this.handleChangeColor} onClick={this.cambiarTallasSegunColor}>
                                    {productos.map((prod, i) => {
                                        return (
                                            <option key={i} value={prod.color}>{prod.color}</option>
                                        );
                                    })}
                                </select>
                            </div>
                        </div>

                        <div className="divisor"></div>

                        {/* TALLA */}
                        <div className="boxTallas">
                            <span>Tallas</span>
                            <div className="opc_sizes">
                                <select name="talla" id="talla" defaultValue={this.state.tallaDefaultProp} onChange={this.handleChangeTalla}>
                                    {productos.map((tal) => {
                                        return (tal.sizes.map((ta, it) => {
                                            return (
                                                (tal.color === this.state.colorSeleccionadoSelect
                                                    ? <option key={it} value={ta.size}> {ta.size}</option>
                                                    : null
                                                )
                                            );
                                        }))
                                    })}
                                </select>
                            </div>
                        </div>
                    </div>
                    : null
                }
            </Fragment>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        cambiarT: state.cambiarT,
        cambiarC: state.cambiarC,
        colorSeleccionado: state.colorS.colorSeleccionado,
        // skuD: state.skuDS.skuDefe,
        // skuS: state.skuDS.skuSel
    }
}

export default connect(mapStateToProps, {
    cambiarTalla, cambiarColor, colorSeleccionado
    //  skuSeleccionado, skuDefault
})(TallaColorComponent)