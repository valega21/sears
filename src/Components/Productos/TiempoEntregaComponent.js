import React, { Component, Fragment } from 'react';

class TiempoEntregaComponent extends Component {
    render() {
        const { collections } = this.props

        return (
            <Fragment>
                {collections.map((col, i) => {
                    return (
                        (col.action === "delivery" ?
                            <div key={i} className="texto-carrito" style={{ width: "100%", overflow: "hidden" }}>
                                <div className="texto1" style={{ width: "15%", display: "flex", float: "left", marginTop: "15px" }}>
                                    <i><img alt="carrito" style={{ width: "35px", height: "auto" }} src={col.img}></img></i>
                                </div>

                                <div className="texto2" style={{ width: "85%", display: "flex", float: "right" }}>
                                    <h2>{col.title}</h2>
                                </div>

                            </div > :
                            null
                        )
                    );
                }
                )}

                <div className="seleccionaEnvos">
                    <div className="btn-envios">
                        Calcular tiempo de entrega
                    </div>
                    <input id="productoId" style={{ display: 'none' }}></input>
                </div>

                <div className="ingresaCP show">
                    <h3>Ingresa tu Código postal:</h3>
                    <input id="cpEnvio" type="text" placeholder="Código Postal" minLength="5" maxLength="5"></input>
                    <input type="hidden" id="cpEnvioReal"></input>
                    <div className="boxTiempo"></div>
                    {/* clase active para mostrar la ventana */}
                    <div className="ventana_emergente" id="errorCPcuenta">Intenta con otro código postal</div>
                    <ul className="autocompleteCP">
                        <li id="licp"></li>
                    </ul>

                    <div className="btn-rojo" id="codigopostal">Calcular</div>
                </div>

                <div className="resultadosCP">
                    <dl>
                        <dt style={{ display: 'none' }} id="labelFecha">Tiempo estimado de entrega: </dt>
                        <dd style={{ display: 'none' }} id="fecha"></dd>
                    </dl>
                    <ul>
                        <li id="cpEncontrado"></li>
                        <li id="delEncontrado"></li>
                    </ul>
                    <div className="link">Modificar Código Postal</div>
                    <p className="small">* Una vez autorizado el pago</p>
                </div>
            </Fragment>
        );
    }
}

export default TiempoEntregaComponent;