import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

export default class seccionenTiendas extends Component {

    render() {
        const { tipo, handleClick} = this.props

        return (
            <Fragment>
                {tipo === 10
                    ?
                    <Fragment>
                        <div className="purchase_btns">
                            <form>
                                <div className="fieldset">
                                    {/* <button type="button" className="recogeEnTienda" onClick={handleClick}>Buscar Producto en Tienda Sears</button> */}
                                    <button type="button" className="recogeEnTienda" onClick={handleClick}>Buscar Producto en Tienda Sears</button>
                                </div>
                            </form>
                        </div>

                        <div className="tarjetaSears">
                            <div className="cont_tarS">
                                <p>Aprovecha los mejores descuentos y promociones utilizando tu Tarjeta Sears!</p>
                                <p className="min-space-top">Si no la tienes, <Link className="a" to="/credito-sears/requisitos-y-formato-de-solicitud" target="_blank">Solicítala AQUI</Link></p>
                            </div>
                        </div>
                    </Fragment>
                    :
                    null
                }
            </Fragment>
        );
    }
} 