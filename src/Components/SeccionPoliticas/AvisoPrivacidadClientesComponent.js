import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDesc } from '../../Redux/Actions/actionGetHelpDesc'
import { showTitulos } from '../../Redux/Actions/actionPoliticas-titulos'

class AvisoPrivacidadClientesComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDesc(932);
        this.props.showTitulos(932);
    }

    componentDidUpdate() {
        const { titulos } = this.props

        this.props.titulo(titulos.subtitle)
    }

    render() {
        const { tipoPolitica } = this.props

        const mapeo = tipoPolitica.map((politica, i) => {
            return (
                <p key={i}>{politica.text}</p>
            );
        })

        return (
            <Fragment>
                {mapeo}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        tipoPolitica: state.tipoPolitica.list,
        titulos: state.tipoPolitica.list2
    }
}

export default connect(mapStateToProps, { showGetHelpDesc, showTitulos })(AvisoPrivacidadClientesComponent)