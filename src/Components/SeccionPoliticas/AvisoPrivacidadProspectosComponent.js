import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDesc } from '../../Redux/Actions/actionGetHelpDesc'
import { showTitulos } from '../../Redux/Actions/actionPoliticas-titulos'

class AvisoPrivacidadProspectosComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDesc(933);
        this.props.showTitulos(933);
    }

    componentDidUpdate() {
        const { titulos } = this.props

        this.props.titulo(titulos.subtitle)
    }

    render() {
        const { prospectos } = this.props

        const mapeo = prospectos.map((politica, i) => {
            return (
                <p key={i}>{politica.text}</p>
            );
        })
        
        return (
            <Fragment>
                {mapeo}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        prospectos: state.tipoPolitica.list,
        titulos: state.tipoPolitica.list2
    }
}

export default connect(mapStateToProps, { showGetHelpDesc, showTitulos })(AvisoPrivacidadProspectosComponent)