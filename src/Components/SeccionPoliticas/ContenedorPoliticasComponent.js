import React, { Component, Fragment } from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import Terminos from './TerminosCondicionesComponent'
import AvisoClientes from './AvisoPrivacidadClientesComponent'
import AvisoProspectos from './AvisoPrivacidadProspectosComponent'
import PoliticasCancelacion from './PoliticasCancelacionComponent'
import PoliticasDevolucion from './PoliticasDevolucionComponent'
import PoliticasEnvio from './PoliticasEnvioComponent'
import PoliticasCompra from './PoliticasCompraComponent'
import { connect } from 'react-redux'
import { showGetHelp } from '../../Redux/Actions/actionGetHelp.js'
import './ContenedorPoliticasComponent.sass'

class ContenedorPoliticasComponent extends Component {

    constructor() {
        super();
        this.state = {
            idSeccion: 0,
            titulo: "",
            politicas: [],
            links:
                ["/terminos-y-condiciones", "/Aviso_Privacidad", "/aviso-privacidad-prospectos", "/politicas-de-cancelacion",
                    "/politicas-de-devolucion", "/politicas-de-envio", "/politicas-de-compra"]
        };
    }

    seleccionSeccion2 = (id) => this.setState({ idSeccion: id })

    seleccionTitulo = (subitem) => this.setState({ titulo: subitem })

    componentDidMount() {
        this.props.showGetHelp(930);
    }

    render() {
        const { politicas } = this.props

        const mapeoPoliticas = politicas.map((tipos, i2) => {
            return (
                <Fragment key={i2}>
                    <li onClick={() => this.seleccionSeccion2(tipos.id)}><Link className="link" to={this.state.links[i2]}>{tipos.subitem}</Link></li>
                </Fragment>
            )
        })

        return (
            <Fragment>
                <section className="sec__Internas">
                    <div className="container">
                        <h1>{this.state.titulo}</h1>
                    </div>
                </section>

                <section className="mod__Interna">
                    <div className="contenedor" >
                        <nav className="menu__Lateral " >
                            <ul>
                                <li><p>Políticas </p></li>
                                {mapeoPoliticas}
                            </ul>
                        </nav>
                        <div className="info__Lateral">
                            <Switch>
                                <Route exact path="/terminos-y-condiciones">
                                    <Terminos idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>

                                <Route exact path="/Aviso_Privacidad">
                                    <AvisoClientes idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>

                                <Route exact path="/aviso-privacidad-prospectos">
                                    <AvisoProspectos idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>

                                <Route exact path="/politicas-de-cancelacion">
                                    <PoliticasCancelacion idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>

                                <Route exact path="/politicas-de-devolucion">
                                    <PoliticasDevolucion idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>

                                <Route exact path="/politicas-de-envio">
                                    <PoliticasEnvio idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>

                                <Route exact path="/politicas-de-compra">
                                    <PoliticasCompra idSeccion={this.state.idSeccion} titulo={this.seleccionTitulo} />
                                </Route>
                            </Switch>
                        </div>
                    </div>
                </section>
            </Fragment >
        );
    }
}

function mapStateToProps(state) {
    return {
        politicas: state.getHelp.list
    }
}

export default connect(mapStateToProps, { showGetHelp })(ContenedorPoliticasComponent)