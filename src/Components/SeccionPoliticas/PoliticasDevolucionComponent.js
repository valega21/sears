import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDesc } from '../../Redux/Actions/actionGetHelpDesc'
import { showTitulos } from '../../Redux/Actions/actionPoliticas-titulos'

class PoliticasDevolucionComponent extends Component {

    componentDidMount() {
        this.props.showTitulos(935);
        this.props.showGetHelpDesc(935);
    }

    componentDidUpdate() {
        const { titulos } = this.props

        this.props.titulo(titulos.subtitle)
    }

    render() {
        const { tipoPoliticas } = this.props

        const mapeo = tipoPoliticas.map((politica, i) => {
            return (
                <p key={i}>{politica.text}</p>
            );
        })

        return (
            <Fragment>
                {mapeo}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        tipoPoliticas: state.tipoPolitica.list,
        titulos: state.tipoPolitica.list2
    }
}

export default connect(mapStateToProps, { showGetHelpDesc, showTitulos })(PoliticasDevolucionComponent)