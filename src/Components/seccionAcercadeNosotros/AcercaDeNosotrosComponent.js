import React, { Component, Fragment } from 'react'

class AcercaDeNosotrosComponent extends Component {
    
    render() {
        return (
            <Fragment>
                <p>La historia de SEARS comienza en 1886 cuando un joven de 22 años llamado Richard Sears, que era empleado en la Estación de Trenes de Nort Redwood, Minnesota, recibió un embarque de relojes para un joyero local. Este se negó a recibir el pedido y Richard Sears, en lugar de devolverlo, como lo hubiera hecho cualquier persona sin visión comercial, lo compró y vendió cada uno de los relojes obteniendo una pequeña utilidad. Animado por este éxito, Sears decide dejar su trabajo de la Estación de Trenes y comenzar un negocio de ventas de relojes por correo.</p>
                <p>El joven Sears decide trasladarse a una ciudad más grande: Chicago. En respuesta a un anuncio que puso en el periódico, contrató los servicios de un relojero llamado Alva C. Roebuck. Así nace el nombre de la firma que nos distingue como empresa "SEARS ROEBUCK".</p>
                <p>En 1899, Roebuck se retira por cuestiones de salud. En ese mismo año, Sears se asocia con un  empresario y fabricante de ropa, cuyo nombre es: Julius Rosenwald. Durante este período el famoso "Catalogo SEARS" comenzó a diversificarse de tal forma que era posible adquirir casi cualquier tipo de mercancía mediante este sistema. En 1908, Sears se retira, le vende su participación a Rosenwald quien asciende a la presidencia. En 1925, las actividades de los negocios se expandieron a los campos de ventas al menudeo mediante la intervención del General Robert E. Wood, quien se había asociado el año anterior.</p>
                <p>La primera tienda de ventas al menudeo se abrió en Chicago. Los siguientes años, continuó creciendo y en la década de los cuarenta, se consolidó como una empresa internacional al iniciar sus operaciones en México, Canadá y Puerto Rico.</p>
                <p>El 27 de febrero de 1947, Sears abre su primer tienda en la Ciudad de México (79-101), en la  Av. Insurgentes, captando la atención de los clientes con su lema:</p>

                <h3>“La completa satisfacción o la devolución de su dinero”</h3>

                <p>El 28 de abril de 1997, Grupo CARSO compra el 85% de las acciones en México. Esta nueva etapa contribuyó al desarrollo sostenido de la Empresa, aportando así una nueva filosofía de trabajo.</p>
                <p>En la actualidad Sears es una empresa 100% mexicana y cuenta con más de setenta y cinco  tiendas en todo el país. Ofrece servicios básicos de reparación de  automóviles  y contratos de servicio para los aparatos electrodomésticos que vende.</p>
                <p>El abastecimiento de mercancías lo obtiene en su gran mayoría de proveedores nacionales. La selección de los mismos, se realiza con base en factores de calidad, servicio y precio. Básicamente las industrias con mayor prestigio en ropa, calzado, muebles y aparatos para el hogar, son proveedores de Sears México.</p>
                <p>La compra de mercancía de importación se realiza en Estados Unidos adquiriendo aparatos para el hogar y herramientas con marcas propias (Kenmore y Crafstman). También compra en Oriente, aprovechando la variedad y novedad de productos de calidad a buen precio.</p>

                <h2>Misión</h2>
                <p>Ofrecer a nuestros clientes los productos y servicios de la más alta calidad, al precio justo, en el ámbito adecuado, procurando su más amplia satisfacción a través de un esmerado servicio personalizado. El cliente es la razón de ser de nuestro trabajo.</p>

                <h2>Visión</h2>
                <p>Consolidar y mantener el liderazgo de nuestra Empresa en el mercado, integrando los objetivos de sus clientes, personal, proveedores y accionistas.</p>

                <h2>Valores</h2>
                <ul>
                    <li>Trabajo</li>
                    <li>Crecimiento</li>
                    <li>Responsabilidad Social</li>
                    <li>Eficiencia</li>
                </ul>
            </Fragment>
        );
    }
}
export default AcercaDeNosotrosComponent