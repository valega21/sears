import React, { Component, Fragment } from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import Tiendas from './SeccionTiendasComponent'
import Nosotros from './AcercaDeNosotrosComponent'
import './ContenedorAcercadeNosotrosComponent.sass'

class ContenedorAcercadeNosotrosComponent extends Component {
    
    render() {
        return (
            <Fragment>
                <section className="sec__Interna">
                    <h1>Acerca de Nosotros</h1>
                </section>
                <section className="mod__Interna">
                    <div className="containers">
                        <nav className="menu__Lateral">
                            <ul>
                                <li>Nuestra Compañía</li>
                                <li><Link to="/acerca-de-nosotros">Acerca de Nosotros</Link></li>
                                <li><Link to="/localizador-tiendas">Localizador de Tiendas</Link></li>
                                <li><a href="http://facturaelectronica.sears.com.mx/">Facturación Eléctronica</a></li>
                                <li><a href="https://wap-mesa-regalos.release.sears.com.mx/">Mesa de Regalos</a></li>
                            </ul>
                        </nav>
                        <div className="info__Lateral">
                            <Switch>
                                <Route exact path="/localizador-tiendas">
                                    <Tiendas />
                                </Route>
                                <Route exact path="/acerca-de-nosotros">
                                    <Nosotros />
                                </Route>
                            </Switch>
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default ContenedorAcercadeNosotrosComponent