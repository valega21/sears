import React, { Component, Fragment } from 'react'

class DireccionTiendaComponent extends Component {

    render() {
        const { data, idTienda } = this.props;

        return (
            <Fragment>
                {data.map((estado, i) => {
                    const tiendas = Object.values(estado.stores);
                    return (
                        tiendas.map((tienda) => {
                            const direcciones = Object.values(tienda.address_content);
                            console.log("tienda: ", tienda.address_content.iframe)
                            const iframe = tienda.address_content.iframe
                            return (
                                (idTienda === i ?
                                    <Fragment key={tienda.id}>
                                        <div className="contDir" key={i}>
                                            <h2> {tienda.address_content.location}</h2>

                                            {direcciones.map((direccion, index) => {
                                                
                                                return (
                                                    <Fragment key={index}>
                                                        {((index <= 5) && (index !== 0)) ?
                                                            <Fragment key={index}>
                                                                {index === 1 || index === 2 ? <p>{direccion}</p> : null}
                                                                {index === 3 ? <p>C.P. {direccion}</p> : null}
                                                                {index === 4 ? <Fragment> <p>{estado.state}</p><p>{direccion}</p></Fragment> : null}
                                                                {index === 5 ? <p>Tel. {direccion}</p> : null}
                                                            </Fragment>
                                                            : null}
                                                    </Fragment>
                                                )
                                            }
                                            )}
                                        </div>

                                        <div className="contMap"> 
                                            <iframe title="mapa de tienda" src={iframe} />
                                        </div>

                                    </Fragment>
                                    : (parseInt(idTienda) === parseInt(tienda.id) ?
                                        <Fragment key={tienda.id}>
                                            <div className="contDir">
                                                <h2> {tienda.address_content.location}</h2>
                                                {direcciones.map((direccion, index) => {
                                                    return (
                                                        (((index <= 5) && (index !== 0))
                                                            ? <Fragment key={index}>
                                                                {index === 1 || index === 2 ? <p>{direccion}</p> : null}
                                                                {index === 3 ? <p>C.P. {direccion}</p> : null}
                                                                {index === 4 ? <Fragment> <p>{estado.state}</p><p>{direccion}</p></Fragment> : null}
                                                                {index === 5 ? <p>Tel. {direccion}</p> : null}
                                                            </Fragment>
                                                            : null)
                                                    )
                                                }
                                                )}
                                            </div>
                                            <div className="contMap">
                                                <iframe title="mapa de tienda" src={iframe} />
                                            </div>
                                        </Fragment>
                                        : null
                                    )
                                )
                            )
                        }
                        )
                    )
                }
                )}
            </Fragment>
        )
    }
}

export default DireccionTiendaComponent