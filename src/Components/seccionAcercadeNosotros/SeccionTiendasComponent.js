import React, { Component, Fragment } from 'react'
import Tienda from './DireccionTiendaComponent'
import { connect } from 'react-redux'
import { showTiendas } from '../../Redux/Actions/actionTiendas.js'

class SeccionTiendasComponent extends Component {
    constructor() {
        super();
        this.state = {
            event: 0
        };
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        this.setState({ event: event.target.value });
    }

    componentDidMount() {
        this.props.showTiendas();
    }

    render() {
        const { estados } = this.props;

        const mapeoEstados = estados.map((estado, i) => {
            const tiendas = Object.values(estado.stores);
            return (
                tiendas.map((tienda, i) => {
                    return (
                        <option key={i} value={tienda.id}> SEARS - {estado.state} - {tienda.name}</option>
                    )
                }
                )
            )
        })

        return (
            <Fragment>
                <form>
                    <span className="select1" style={{ position: "relative" }}>
                        <select className="form-control" id="sucursal" name="sucursal" value={this.state.value} onChange={this.handleChange}>
                            {mapeoEstados}
                        </select>
                    </span>
                </form>

                <Tienda data={estados} idTienda={this.state.event} />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        estados: state.tienda.list
    }
}

export default connect(mapStateToProps, { showTiendas })(SeccionTiendasComponent)