import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import {
    show_AutocenterTitulos,
    show_AutocenterImages,
    show_AutocenterTemplate,
    show_Autocenter811,
    show_Autocenter821
} from '../../Redux/Actions/actionGetHelp'
import './AutoCenterComponent.sass'

class AutoCenterComponent extends Component {

    componentDidMount() {
        this.props.show_AutocenterTitulos()
        this.props.show_AutocenterImages()
        this.props.show_AutocenterTemplate()
        this.props.show_Autocenter811()
        this.props.show_Autocenter821()
    }

    render() {
        const { titulos, imagenes, templates, servicios, direcciones } = this.props
        this.props.titulo(titulos.title)
        const imagenes1 = [imagenes.length - 1]

        const mapeoServicio = imagenes.map((ima, i) => {
            return (
                (i < imagenes1 ?
                    <p key={i}>
                        <img alt="autocenter" src={ima.link} style={{ width: "100%" }} />
                    </p>
                    : null)
            );
        })

        const mapeoVerificacion = imagenes.map((ima, i) => {
            return (
                (i === parseInt(imagenes1) ?
                    <p key={i}>
                        <img alt="verificación" src={ima.link} style={{ width: "100%" }} />
                    </p>
                    : null)
            );
        })
        
        return (
            <Fragment>
                <h2>Nuestro Servicio</h2>
                {mapeoServicio}


                <h2>Verificación</h2>
                {mapeoVerificacion}

                <Fragment>
                    {templates.map((tem, i) => {
                        return (
                            <div key={i}>
                                <h2>{tem.subitem}</h2>
                                {tem.action === "servicelist" ?
                                    <div className="contenedorServicios">
                                        {servicios.map((servi, i) => {
                                            const tipos = Object.values(servi.services)
                                            return (
                                                <div className="tiposServicios" key={i}>
                                                    <div className="letraServicio"><h6>{servi.type}</h6></div>
                                                    {tipos.map((servi, i) => {
                                                        return (
                                                            <ul key={i}>
                                                                <li>
                                                                    <div className="contenedordivs">
                                                                        <div className="divIzquierda" >
                                                                            <img alt="servicios" src={servi.img} />
                                                                        </div>
                                                                        <div className="divDerecha">
                                                                            <p>{servi.text}</p>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        );
                                                    })}

                                                </div>
                                            );
                                        }
                                        )}
                                    </div> : null
                                }

                                {tem.action === "directorylist" ?
                                    <div className="contenedorDirectorio">
                                        {direcciones.map((tem, i) => {
                                            const tiendas = Object.values(tem.stores)
                                            return (
                                                <div className="directorio" key={i} >
                                                    <div className="letraDirectorio">
                                                        <h6>{tem.zone}</h6>
                                                    </div>
                                                    <ul>
                                                        {tiendas.map((tien, i) => {
                                                            return (
                                                                <li key={i}>
                                                                    <div className="textoDirectorio">
                                                                        <h3>{tien.name}</h3>
                                                                        <p>{tien.address}</p>
                                                                        <p>{tien.phone}</p>
                                                                        <p>{tien.mail}</p>
                                                                    </div>
                                                                </li>
                                                            );
                                                        }
                                                        )}
                                                    </ul>
                                                </div>
                                            );
                                        }
                                        )}
                                    </div>
                                    : null
                                }
                            </div>
                        );
                    }
                    )}
                </Fragment>
            </Fragment>
        );

    }
}

function mapStateToProps(state) {
    return {
        titulos: state.getHelp.list9,
        imagenes: state.getHelp.list10,
        templates: state.getHelp.list11,
        servicios: state.getHelp.list12,
        direcciones: state.getHelp.list13
    }
}

export default connect(mapStateToProps, { show_AutocenterTitulos, show_AutocenterImages, show_AutocenterTemplate, show_Autocenter811, show_Autocenter821 })(AutoCenterComponent)