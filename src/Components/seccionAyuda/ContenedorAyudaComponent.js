import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Preguntas from './PreguntasFrecuentesComponent.js'
import FormasPago from './FormasPagoComponent.js'
import Dudas from './DudasComentariosComponent.js'
import AutoCenter from './AutoCenterComponent.js'

class ContenedorAyudaComponent extends Component {

    constructor() {
        super();
        this.state = {
            titulo: "",
        };
    }

    seleccionTitulo = (subitem) => this.setState({ titulo: subitem })

    componentDidMount() {

    }

    render() {
        return (
            <Fragment>
                <section className="sec__Internas">
                    <div className="container">
                        <h1>{this.state.titulo}</h1>
                    </div>
                </section>
                <Router>
                    <section className="mod__Interna">
                        <div className="containers" >
                            <nav className="menu__Lateral" >
                                <ul>
                                    <li><p>Déjanos Ayudarte </p></li>
                                    <li><Link to="/dudas-y-comentarios" className="link">Dudas y Comentarios </Link></li>
                                    <li><Link to="/formas-pago" className="link">Formas de Pago </Link></li>
                                    <li><Link to="/preguntas-frecuentes" className="link">Preguntas Frecuentes</Link></li>
                                    <li><Link to="/auto-center" className="link">AutoCenter </Link></li>
                                </ul>
                            </nav>
                            <div className="info__Lateral">
                                <Switch>
                                    <Route exact path="/dudas-y-comentarios">
                                        <Dudas titulo={this.seleccionTitulo} />
                                    </Route>
                                    <Route exact path="/formas-pago">
                                        <FormasPago titulo={this.seleccionTitulo} />
                                    </Route>
                                    <Route exact path="/preguntas-frecuentes">
                                        <Preguntas titulo={this.seleccionTitulo} />
                                    </Route>
                                    <Route exact path="/auto-center">
                                        <AutoCenter titulo={this.seleccionTitulo} />
                                    </Route>
                                </Switch>
                            </div>
                        </div>
                    </section>
                </Router>
            </Fragment>
        );
    }
}

export default ContenedorAyudaComponent