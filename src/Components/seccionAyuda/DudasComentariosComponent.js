import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showAtencion } from '../../Redux/Actions/actionAtencionClientes'
import { showTitulos } from '../../Redux/Actions/actionGetHelp'

class DudasComentariosComponent extends Component {

    componentDidMount() {
        this.props.showAtencion();
        this.props.showTitulos(940);
    }

    render() {
        const { dudas, titulos } = this.props

        const mapeoDudas = dudas.map((duda, i) => {
            const descripcion = Object.values(duda.description);
            return (
                <Fragment key={i}>
                    {i < 2 ? <h3>{duda.subject}</h3> : null}
                    {descripcion.map((des, i) => {
                        return (
                            <p key={i}>{des.text}</p>
                        );
                    }
                    )}
                </Fragment>
            );
        })

        return (
            <Fragment>
                {this.props.titulo(titulos.title)}

                <div>
                    {mapeoDudas}
                </div>
            </Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        dudas: state.atencion.list,
        titulos: state.atencion.list2
    }
}

export default connect(mapStateToProps, { showAtencion, showTitulos })(DudasComentariosComponent)