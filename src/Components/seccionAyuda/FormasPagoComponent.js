import React, { Component, Fragment } from 'react'
import TarjetasSears from './FormasPagoTarjetaSearsComponent'
import Monedero from './FormasPagoMonederoComponent'
import Paypal from './FormasPagoPaypalComponent'
import TarjetasCredito from './FormasPagoTarjetasCreditoComponent'
import DepositoBancario from './FormasPagoDepositoBancarioComponent'
import Extranjero from './FormasPagoDesdeExtranjeroComponent'
import { connect } from 'react-redux'
import { showGetHelp } from '../../Redux/Actions/actionGetHelp.js'
import { showTitulos } from '../../Redux/Actions/actionGetHelp'

class FormasPagoComponent extends Component {

    componentDidMount() {
        this.props.showGetHelp(920);
        this.props.showTitulos(920);
    }

    render() {
        const { formasPago, titulos } = this.props
        this.props.titulo(titulos.title)

        const mapeoFormasPago = formasPago.map((tipo, i) => {
            return (
                <li key={i}>{tipo.subitem}</li>
            );
        })

        return (
            <Fragment>
                <p>{titulos.subtitle}</p>
                <ul>
                    {mapeoFormasPago}
                </ul>
                <TarjetasSears />
                <Monedero />
                <Paypal />
                <TarjetasCredito />
                <DepositoBancario />
                <Extranjero />
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        formasPago: state.getHelp.list,
        titulos: state.atencion.list2
    }
}

export default connect(mapStateToProps, { showGetHelp, showTitulos })(FormasPagoComponent)