import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDescDeposito } from '../../Redux/Actions/actionGetHelpDesc'
import { showGetHelpPagosDeposito } from '../../Redux/Actions/formas-pago/actionGetHelpTiposPagos'

class FormasPagoDepositoBancarioComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDescDeposito(925)
        this.props.showGetHelpPagosDeposito(925)
    }

    render() {
        const { titulos, datos } = this.props

        const mapeoDatos = datos.map((des, i) => {
            return (
                <p key={i}>{des.text}</p>
            );
        })

        return (
            <Fragment>
                <h3>{titulos.subtitle}</h3>
                {mapeoDatos}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        datos: state.tipoPolitica.list7,
        titulos: state.getHelp.list7
    }
}

export default connect(mapStateToProps, { showGetHelpDescDeposito, showGetHelpPagosDeposito })(FormasPagoDepositoBancarioComponent)