import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDescExtranjero } from '../../Redux/Actions/actionGetHelpDesc'
import { showGetHelpPagosExtranjero } from '../../Redux/Actions/formas-pago/actionGetHelpTiposPagos'

class FormasPagoExtranjeroComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDescExtranjero(926)
        this.props.showGetHelpPagosExtranjero(926)
    }

    render() {
        const { titulos, datos } = this.props

        const mapeoDatos = datos.map((des, i) => {
            return (
                <p key={i}>{des.text}</p>
            );
        })

        return (
            <Fragment>
                <h3>{titulos.subtitle}</h3>
                {mapeoDatos}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        datos: state.tipoPolitica.list8,
        titulos: state.getHelp.list8
    }
}

export default connect(mapStateToProps, { showGetHelpDescExtranjero, showGetHelpPagosExtranjero })(FormasPagoExtranjeroComponent)