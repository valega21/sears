import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDescMonedero } from '../../Redux/Actions/actionGetHelpDesc'
import { showGetHelpPagosMonedero } from '../../Redux/Actions/formas-pago/actionGetHelpTiposPagos'

class FormasPagoMonederoComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDescMonedero(922)
        this.props.showGetHelpPagosMonedero(922)
    }

    render() {
        const { datos, titulos } = this.props

        const mapeoDatos = datos.map((des, i) => {
            return (
                <p key={i}>{des.text}</p>
            );
        })
        
        return (
            <Fragment>
                <h3>{titulos.subtitle}</h3>

                {mapeoDatos}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        datos: state.tipoPolitica.list4,
        titulos: state.getHelp.list4
    }
}

export default connect(mapStateToProps, { showGetHelpDescMonedero, showGetHelpPagosMonedero })(FormasPagoMonederoComponent)