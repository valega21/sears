import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDescPaypal } from '../../Redux/Actions/actionGetHelpDesc'
import { showGetHelpPagosPaypal } from '../../Redux/Actions/formas-pago/actionGetHelpTiposPagos'

class FormasPagoPaypalComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDescPaypal(923)
        this.props.showGetHelpPagosPaypal(923)
    }

    render() {
        const { titulos, datos } = this.props

        const mapeoDatos = datos.map((des, i) => {
            return (
                <p key={i}>{des.text}</p>
            );
        })
        
        return (
            <Fragment>
                <h3>{titulos.subtitle}</h3>

                {mapeoDatos}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        datos: state.tipoPolitica.list5,
        titulos: state.getHelp.list5
    }
}

export default connect(mapStateToProps, { showGetHelpDescPaypal, showGetHelpPagosPaypal })(FormasPagoPaypalComponent)