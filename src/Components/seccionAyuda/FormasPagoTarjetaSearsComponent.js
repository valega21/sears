import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDescTarjetaSears } from '../../Redux/Actions/actionGetHelpDesc'
import { showGetHelpPagosTarjetasSears } from '../../Redux/Actions/formas-pago/actionGetHelpTiposPagos'

class FormasPagoTarjetasSearsComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDescTarjetaSears(921)
        this.props.showGetHelpPagosTarjetasSears(921)
    }

    render() {
        const { datos, tituloss } = this.props

        const mapeoDatos = datos.map((des, i) => {
            return (
                <p key={i}>{des.text}</p>
            );
        })

        return (
            <Fragment>
                <h3>{tituloss.subtitle}</h3>
                {mapeoDatos}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        datos: state.tipoPolitica.list3,
        tituloss: state.getHelp.list3
    }
}

export default connect(mapStateToProps, { showGetHelpDescTarjetaSears, showGetHelpPagosTarjetasSears })(FormasPagoTarjetasSearsComponent)