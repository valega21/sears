import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showGetHelpDescTarjetasCredito } from '../../Redux/Actions/actionGetHelpDesc'
import { showGetHelpPagosTarjetasCredito } from '../../Redux/Actions/formas-pago/actionGetHelpTiposPagos'

class FormasPagoTarjetasCreditoComponent extends Component {

    componentDidMount() {
        this.props.showGetHelpDescTarjetasCredito(924)
        this.props.showGetHelpPagosTarjetasCredito(924)
    }

    render() {
        const { titulos, datos } = this.props

        const mapeoDatos = datos.map((des, i) => {
            return (
                <p key={i}>{des.text}</p>
            );
        })

        return (
            <Fragment>
                <h3>{titulos.subtitle}</h3>

                {mapeoDatos}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        datos: state.tipoPolitica.list6,
        titulos: state.getHelp.list6
    }
}

export default connect(mapStateToProps, { showGetHelpDescTarjetasCredito, showGetHelpPagosTarjetasCredito })(FormasPagoTarjetasCreditoComponent)
