import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showTitulos, showGetHelp_Preguntas } from '../../Redux/Actions/actionGetHelp'

class PreguntasFrecuentesComponent extends Component {
    componentDidMount() {
        this.props.showTitulos(910);
        this.props.showGetHelp_Preguntas(910);
    }

    render() {
        const { titulos, preguntas } = this.props
        this.props.titulo(titulos.title);

        const mapeoPreguntas = preguntas.map((opcion, i) => {
            return (
                <Fragment key={i}>
                    <h2>{opcion.subject}</h2>
                    {opcion.description.map((des, i) => {
                        return (
                            <p key={i}>{des.text}</p>
                        );
                    }
                    )}
                </Fragment>
            );
        })

        return (
            <Fragment>
                {mapeoPreguntas}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        titulos: state.atencion.list2,
        preguntas: state.getHelp.list2
    }
}

export default connect(mapStateToProps, { showGetHelp_Preguntas, showTitulos })(PreguntasFrecuentesComponent)