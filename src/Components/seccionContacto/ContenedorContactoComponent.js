import React, { Component, Fragment } from 'react'
import Contacto from './TextoContactoComponent.js'
import './ContenedorContactoComponent.sass'

class ContenedorContactoComponent extends Component {
    constructor() {
        super();
        this.state = {
            titulo: ""
        };
    }

    seleccionTitulo = (title) => this.setState({ titulo: title })

    render() {
        return (
            <Fragment>
                <section className="sec__Internas">
                    <div className="containe" style={{ position: "relative", right: "1%" }}>
                        <h1>{this.state.titulo}</h1>
                    </div>
                </section>

                <section className="mod__Interna" >
                    <div className="containe" >
                        <div className="info__Completa" style={{ float: "left", position: "relative", right: "4%", width: "100%" }}>
                            <Contacto titulo={this.seleccionTitulo} />
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default ContenedorContactoComponent