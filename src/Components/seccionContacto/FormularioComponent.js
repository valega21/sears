import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { showCombo, showState } from '../../Redux/Actions/actionContacto-formulario'

class FormularioComponent extends Component {

    componentDidMount() {
        this.props.showCombo();
        this.props.showState();
    }

    render() {
        const { combos, estados } = this.props;

        const mapeoCombos = combos.map((area, i) =>
            <option key={i} defaultValue={area.area}>{area.area}</option>
        )

        const mapeoEstados = estados.map((area, i) =>
            <option key={i} defaultValue={area.id}>{area.state}</option>
        )

        return (
            <Fragment>
                <fieldset >
                    <dl className="form-group">
                        <dt><label>Área que desea Contactar: </label></dt>
                        <dd>
                            <span className="select">
                                <select className="form-control ">
                                    <option selected> -No seleccionado-</option>
                                    {mapeoCombos}
                                </select>
                            </span>
                        </dd>
                    </dl>
                    <dl>
                        <dt><label>Nombre: </label></dt>
                        <dd><input type="text" placeholder="Ingresa tu nombre" name="name" required /></dd>
                    </dl>
                    <dl>
                        <dt><label> Apellido Paterno: </label></dt>
                        <dd><input type="text" placeholder="Ingresa tu Apellido Paterno" name="firstname" required /></dd>
                    </dl>

                    <dl>
                        <dt><label> Apellido Materno: </label></dt>
                        <dd><input type="text" placeholder="Ingresa tu Apellido Materno" name="firstname" required /></dd>
                    </dl>
                    <dl>
                        <dt><label> Correo Electrónico: </label></dt>
                        <dd><input type="email" placeholder="Ingresa tu Correo Electrónico" name="mail" required /></dd>
                    </dl>
                    <dl className="form-group">
                        <dt><label>Estado: </label></dt>
                        <dd>
                            <span className="select">
                                <select className="form-control ">
                                    <option selected>-No Seleccionado-</option>
                                    {mapeoEstados}
                                </select>
                            </span>
                        </dd>
                    </dl>
                    <dl>
                        <dt><label> Código Postal: </label></dt>
                        <dd><input type="text" placeholder="Ingresa tu C.P." name="cp" /></dd>
                    </dl>

                    <dl>
                        <dt><label>Comentario:</label></dt>
                        <dd><textarea placeholder="Ingresa un comentario"></textarea></dd>
                    </dl>

                    <fieldset>
                        <button type="submit" className="btn-rojo next" >
                            Enviar mensaje
                        </button>
                    </fieldset>
                </fieldset>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        combos: state.combo.list,
        estados: state.combo.list2
    }
}

export default connect(mapStateToProps, { showCombo, showState })(FormularioComponent)