import React, { Component, Fragment } from 'react'
import Formulario from './FormularioComponent.js'
import { connect } from 'react-redux'
import { showAtencion } from '../../Redux/Actions/actionAtencionClientes'

class TextoContactoComponent extends Component {

    componentDidMount() {
        this.props.showAtencion();
    }

    render() {
        const { atencions } = this.props;

        return (
            <Fragment>
                {atencions.map((dat, i) => {
                    const textos = Object.values(dat.description)
                    return (
                        <Fragment key={i}>
                            {i < (atencions.length - 1) ?
                                <Fragment>
                                    {i > 0 ? <h3>{dat.subject}</h3> : this.props.titulo(dat.subject)}

                                    {textos.map((tex, i) => {
                                        return (
                                            <p key={i}>{tex.text}</p>
                                        );
                                    }
                                    )}
                                </Fragment>

                                : <form className="contactoMail">
                                    <h2>{dat.subject}</h2>
                                    <p>{dat.title}</p>
                                    <Formulario />
                                </form>
                            }
                        </Fragment>
                    )
                }
                )}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        atencions: state.atencion.list
    }
}

export default connect(mapStateToProps, { showAtencion })(TextoContactoComponent)