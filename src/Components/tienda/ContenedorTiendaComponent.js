import React, { Component, Fragment } from 'react'

class ContenedorTiendaComponent extends Component {
    render() {
        return (
            <Fragment>
                <section>
                    <div className="sec__headerTiendas">
                        <img alt="imagen1" src=""/>
                    </div>
                </section>

                <section className="content-categories">
                    <div className="sec__conent">
                        <div className="storeFilters">
                            <aside className="idTiendaBasica">
                                <div className="nameImgBasicStore">
                                    <div className="imgStore">
                                        <img alt="imagen2" src=""/>
                                    </div>
                                    <div className="nameStore">
                                        <p></p>
                                    </div>
                                </div>
                            </aside>

                            <aside className="conent__filtro">
                                <div className="Cont__search">

                                </div>
                                <div className="agrupaFilter">
                                    <div className="menuCategorias subCat active">
                                        <h3 className="categ title">
                                            <a href="#!">texto</a>
                                        </h3>
                                        <nav className="top-nav-collapse">
                                            <dl>
                                                <dt>
                                                    <a href="#!">text</a>
                                                </dt>
                                            </dl>
                                        </nav>
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <div className="conent__productos tiendaBMar"></div>
                    </div>
                </section>
            </Fragment>
        );
    }
}
export default ContenedorTiendaComponent