import axios from 'axios';
export const SHOW_CREDITO = 'SHOW_CREDITO'
export const SHOW_CREDITO_IMG = 'SHOW_CREDITO_IMG'

export function showCredito() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getInfo/4')
            .then((response) => {
                dispatch({ type: SHOW_CREDITO, payload: response.data.data.content.ul })
            })
    }
}

export function showCreditoImg() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getInfo/4')
            .then((response) => {
                dispatch({ type: SHOW_CREDITO_IMG, payload: response.data.data })
            })
    }
}