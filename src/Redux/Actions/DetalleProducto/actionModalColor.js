export const CAMBIAR_COLOR = 'CAMBIAR_COLOR';
export const COLOR_SELECCIONADO = 'COLOR_SELECCIONADO'

export function cambiarColor(color) {
    return(dispatch)=>{
        dispatch({type: CAMBIAR_COLOR, payload: color})
    }
}

export function colorSeleccionado(evento) {
    return(dispatch)=>{
        dispatch({type: COLOR_SELECCIONADO, payload: evento})
    }
}