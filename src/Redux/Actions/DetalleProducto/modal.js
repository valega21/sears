export const CAMBIAR_TALLA = 'CAMBIAR_TALLA'

export function cambiarTalla(talla) {
    return(dispatch)=>{
        dispatch({type: CAMBIAR_TALLA, payload: talla})
    }
}