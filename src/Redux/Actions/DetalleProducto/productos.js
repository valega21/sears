import axios from 'axios'
export const SHOW_PRODUCTO_POLO = 'SHOW_PRODUCTO_POLO'
export const SHOW_PRODUCTO = 'SHOW_PRODUCTO'
export const SHOW_PRODUCTO_IMAGENES = 'SHOW_PRODUCTO_IMAGENES'
export const SHOW_PRODUCTO_PAGOS = 'SHOW_PRODUCTO_PAGOS'
export const SHOW_POLITICA_PRODUCTO = 'SHOW_POLITICA_PRODUCTO'
export const SHOW_PRODUCTO_ATRIBUTO = 'SHOW_PRODUCTO_ATRIBUTO'
export const SHOW_PRODUCTO_SIMILARES = 'SHOW_PRODUCTO_SIMILARES'
export const SHOW_PROMOCION_TARJETA = 'SHOW_PROMOCION_TARJETA'
export const SHOW_PROMOCION = 'SHOW_PROMOCION'
export const SHOW_COLLECTIONS = 'SHOW_COLLECTIONS'
export const SHOW_SHIPPING = 'SHOW_SHIPPING'
export const SHOW_MARCA = 'SHOW_MARCA'
export const SHOW_TIPO_PRODUCTO = 'SHOW_TIPO_PRODUCTO'
export const SHOW_VENDIDO_TIENDA = 'SHOW_VENDIDO_TIENDA'
export const SHOW_DISPONIBILIDAD_TIENDA = 'SHOW_DISPONIBILIDAD_TIENDA'
export const SHOW_COLOR = ' SHOW_COLOR'
export const SHOW_TALLADEFAULT = 'SHOW_TALLADEFAULT'
export const SKU_DEFAULT = 'SKU_DEFAULT'
export const SKU_SELECCIONADO = 'SKU_SELECCIONADO'
const urlPromociones = "http://clauluna.com/CLARO/api/getPromociones_P?id="
const url = 'http://clauluna.com/CLARO/api/getDetalleProductoSears/'
const urlDisponibilidadSeteada = 'http://clauluna.com/CLARO/api/getDisponibilidadTienda?skuP='
const concatenar = '&skuH='
const skuH = '87691665'
// const urlDisponibilidad = 'http://clauluna.com/CLARO/api/getDisponibilidadTienda?skuP=124279&skuH=87691665'

//tipo de producto: internet, tienda
export function showTipoProducto(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_TIPO_PRODUCTO, payload: response.data.data.type_product.id })
            })
    }
}
// producto en tienda
export function showVendidoTienda(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_VENDIDO_TIENDA, payload: response.data.data.type_product.content })
            })
    }
}
// disponibilidad de producto en tienda
export function showDisponibilidad(skuP) {
    return (dispatch, getState) => {
        axios.get(urlDisponibilidadSeteada + skuP + concatenar + skuH)
            .then((response) => {
                dispatch({ type: SHOW_DISPONIBILIDAD_TIENDA, payload: response.data.data.content })
            })
    }
}

//detalle Producto
export function showProducto(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_PRODUCTO, payload: response.data.data })
            })
    }
}

export function showProductoImagenes(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_PRODUCTO_IMAGENES, payload: response.data.data.images })
            })
    }
}

export function showProductoPolo(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_PRODUCTO_POLO, payload: response.data.data.colors })
            })
    }
}

export function color(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_COLOR, payload: response.data.data.colors[0].color })
            })
    }
}

export function tallaDefault(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_TALLADEFAULT, payload: response.data.data.colors[0].sizes[0].size })
            })
    }
}

export function skuSeleccionado(sku) {
    return (dispatch) => {
        dispatch({ type: SKU_SELECCIONADO, payload: sku })
    }
}

export function skuDefault(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SKU_DEFAULT, payload: response.data.data.colors[0].sizes[0].sku })
            })
    }
}

export function showProductoPagos(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_PRODUCTO_PAGOS, payload: response.data.data.tabs })
            })
    }
}

export function showProductoAtributo(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_PRODUCTO_ATRIBUTO, payload: response.data.data.attr })
            })
    }
}

export function showMarca(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_MARCA, payload: response.data.data.attributes })
            })
    }
}

export function showProductoSimilares(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_PRODUCTO_SIMILARES, payload: response.data.data.carousel.products })
            })
    }
}

export function showPoliticaProducto() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getPoliticas_P?id=56')
            .then((response) => {
                dispatch({ type: SHOW_POLITICA_PRODUCTO, payload: response.data.data.content })
            })
    }
}

export function showCollections(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_COLLECTIONS, payload: response.data.data.collections })
            })
    }
}
export function showShippingFree(id) {
    return (dispatch, getState) => {
        axios.get(url + id)
            .then((response) => {
                dispatch({ type: SHOW_SHIPPING, payload: response.data.data.isshipping })
            })
    }
}

export function showPromocionTarjeta() {
    return (dispatch, getState) => {
        axios.get(urlPromociones + 2)
            .then((response) => {
                dispatch({ type: SHOW_PROMOCION_TARJETA, payload: response.data.data.content })
            })
    }
}

export function showPromocion() {
    return (dispatch, getState) => {
        axios.get(urlPromociones + 1)
            .then((response) => {
                dispatch({ type: SHOW_PROMOCION, payload: response.data.data.content })
            })
    }
}