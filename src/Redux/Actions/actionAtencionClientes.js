import axios from 'axios';
export const SHOW_ATENCION = 'SHOW_ATENCION'

export function showAtencion() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getHelp/940')
            .then((response) => {
                dispatch({ type: SHOW_ATENCION, payload: response.data.data.content })
            })
    }
}