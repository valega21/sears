import axios from 'axios';
export const SHOW_COMBO = 'SHOW_COMBO'
export const SHOW_STATE = 'SHOW_STATE'

export function showCombo(){
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getSelectContacto')
            .then((response) => {
                dispatch({ type: SHOW_COMBO, payload: response.data.data.areas })
            })
    }
}

export function showState(){
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getSelectContacto')
            .then((response) => {
                dispatch({ type: SHOW_STATE, payload: response.data.data.states })
            })
    }
}

