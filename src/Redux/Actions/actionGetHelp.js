import axios from 'axios';
export const SHOW_GETHELP = 'SHOW_GETHELP';
export const SHOW_TITULOS = 'SHOW_TITULOS'
export const SHOW_GETHELP_PREGUNTAS = 'SHOW_GETHELP_PREGUNTAS'
export const SHOW_AUTOCENTER_IMAGES = 'SHOW_AUTOCENTER_IMAGES'
export const SHOW_AUTOCENTER_TITULOS = 'SHOW_AUTOCENTER_TITULOS'
export const SHOW_AUTOCENTER_TEMPLATE = 'SHOW_AUTOCENTER_TEMPLATE'
export const SHOW_AUTOCENTER_SERVICIOS = 'SHOW_AUTOCENTER_SERVICIOS'
export const SHOW_AUTOCENTER_DIRECTORIO = 'SHOW_AUTOCENTER_DIRECTORIO'
const rutaGethelp = "http://clauluna.com/CLARO/api/getHelp/"
const rutaAutocenter = "http://clauluna.com/CLARO/api/getServices/810"
const rutaAutocenter811 = "http://clauluna.com/CLARO/api/getServicesDesc/811"
const rutaAutocenter821 = "http://clauluna.com/CLARO/api/getServicesDesc/821"

export function showGetHelp(id) {
    return (dispatch, getState) => {
        axios.get(rutaGethelp + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP, payload: response.data.data.content })
            })
    }
}

export function showTitulos(id) {
    return (dispatch, getState) => {
        axios.get(rutaGethelp + id)
            .then((response) => {
                dispatch({ type: SHOW_TITULOS, payload: response.data.data })
            })
    }
}

export function showGetHelp_Preguntas(id) {
    return (dispatch, getState) => {
        axios.get(rutaGethelp + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PREGUNTAS, payload: response.data.data.content })
            })
    }
}

export function show_AutocenterTitulos() {
    return (dispatch, getState) => {
        axios.get(rutaAutocenter)
            .then((response) => {
                dispatch({ type: SHOW_AUTOCENTER_TITULOS, payload: response.data.data })
            })
    }
}

export function show_AutocenterImages() {
    return (dispatch, getState) => {
        axios.get(rutaAutocenter)
            .then((response) => {
                dispatch({ type: SHOW_AUTOCENTER_IMAGES, payload: response.data.data.images })
            })
    }
}

export function show_AutocenterTemplate() {
    return (dispatch, getState) => {
        axios.get(rutaAutocenter)
            .then((response) => {
                dispatch({ type: SHOW_AUTOCENTER_TEMPLATE, payload: response.data.data.template })
            })
    }
}

export function show_Autocenter811() {
    return (dispatch, getState) => {
        axios.get(rutaAutocenter811)
            .then((response) => {
                dispatch({ type: SHOW_AUTOCENTER_SERVICIOS, payload: response.data.data.content })
            })
    }
}

export function show_Autocenter821() {
    return (dispatch, getState) => {
        axios.get(rutaAutocenter821)
            .then((response) => {
                dispatch({ type: SHOW_AUTOCENTER_DIRECTORIO, payload: response.data.data.content })
            })
    }
}