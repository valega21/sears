import axios from 'axios';
export const SHOW_GETHELPDESC = 'SHOW_GETHELPDESC'
export const SHOW_GETHELPDESC_TARJETASEARS = 'SHOW_GETHELPDESC_TARJETASEARS'
export const SHOW_GETHELPDESC_MONEDERO = 'SHOW_GETHELPDESC_MONEDERO'
export const SHOW_GETHELPDESC_PAYPAL = 'SHOW_GETHELPDESC_PAYPAL'
export const SHOW_GETHELPDESC_TARJETASCREDITO = 'SHOW_GETHELPDESC_TARJETASCREDITO'
export const SHOW_GETHELPDESC_DEPOSITO = 'SHOW_GETHELPDESC_DEPOSITO'
export const SHOW_GETHELPDESC_EXTRANJERO = 'SHOW_GETHELPDESC_EXTRANJERO'

export function showGetHelpDesc(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC, payload: response.data.data.content })
            })
    }
}

export function showGetHelpDescTarjetaSears(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC_TARJETASEARS, payload: response.data.data.content })
            })
    }
}

export function showGetHelpDescMonedero(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC_MONEDERO, payload: response.data.data.content })
            })
    }
}

export function showGetHelpDescPaypal(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC_PAYPAL, payload: response.data.data.content })
            })
    }
}

export function showGetHelpDescTarjetasCredito(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC_TARJETASCREDITO, payload: response.data.data.content })
            })
    }
}

export function showGetHelpDescDeposito(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC_DEPOSITO, payload: response.data.data.content })
            })
    }
}

export function showGetHelpDescExtranjero(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELPDESC_EXTRANJERO, payload: response.data.data.content })
            })
    }
}