import axios from 'axios';
export const SHOW_MARCAS = 'SHOW_MARCAS'

export function showMarcas() {

    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getBrands')
            .then((response) => {
                dispatch({ type: SHOW_MARCAS, payload: response.data.data })
            })
    }
}