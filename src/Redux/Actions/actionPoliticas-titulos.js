import axios from 'axios';
export const SHOW_TITULOS = 'SHOW_TITULOS'

export function showTitulos(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"
    
    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_TITULOS, payload: response.data.data })
            })
    }
}