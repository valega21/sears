import axios from 'axios';
export const SHOW_TIENDAS = 'SHOW_TIENDAS'

export function showTiendas() {

    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getStores')
            .then((response) => {
                dispatch({ type: SHOW_TIENDAS, payload: response.data.data.content })
            })
    }
}