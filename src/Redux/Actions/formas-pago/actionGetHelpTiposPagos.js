import axios from 'axios';
export const SHOW_GETHELP_PAGOS_TARJETASEARS = 'SHOW_GETHELP_PAGOS_TARJETASEARS';
export const SHOW_GETHELP_PAGOS_MONEDERO = 'SHOW_GETHELP_PAGOS_MONEDERO';
export const SHOW_GETHELP_PAGOS_PAYPAL = 'SHOW_GETHELP_PAGOS_PAYPAL';
export const SHOW_GETHELP_PAGOS_TARJETASCREDITO = 'SHOW_GETHELP_PAGOS_TARJETASCREDITO';
export const SHOW_GETHELP_PAGOS_DEPOSITO = 'SHOW_GETHELP_PAGOS_DEPOSITO'
export const SHOW_GETHELP_PAGOS_EXTRANJERO = 'SHOW_GETHELP_PAGOS_EXTRANJERO'

export function showGetHelpPagosTarjetasSears(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PAGOS_TARJETASEARS, payload: response.data.data })
            })
    }
}

export function showGetHelpPagosMonedero(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PAGOS_MONEDERO, payload: response.data.data })
            })
    }
}

export function showGetHelpPagosPaypal(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PAGOS_PAYPAL, payload: response.data.data })
            })
    }
}

export function showGetHelpPagosTarjetasCredito(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PAGOS_TARJETASCREDITO, payload: response.data.data })
            })
    }
}

export function showGetHelpPagosDeposito(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PAGOS_DEPOSITO, payload: response.data.data })
            })
    }
}

export function showGetHelpPagosExtranjero(id) {
    const ruta = "http://clauluna.com/CLARO/api/getHelpDesc/"

    return (dispatch, getState) => {
        axios.get(ruta + id)
            .then((response) => {
                dispatch({ type: SHOW_GETHELP_PAGOS_EXTRANJERO, payload: response.data.data })
            })
    }
}