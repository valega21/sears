import axios from 'axios';
export const SHOW_HOME_SLIDERPRINCIPAL = 'SHOW_HOME_SLIDERPRINCIPAL'
export const SHOW_HOME_SLIDERSECUNDARIO = 'SHOW_HOME_SLIDERSECUNDARIO'
export const SHOW_HOME_SLIDERTRES = 'SHOW_HOME_SLIDERTRES'

export function showHomeSliderPrincipal() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getHomeSears')
            .then((response) => {
                response.data.data.forEach((element, i) => {
                    if (element.widget === "principalSliderWeb") {
                        dispatch({ type: SHOW_HOME_SLIDERPRINCIPAL, payload: response.data.data[i].object })
                    }
                });
            })
    }
}

export function showHomeSliderSecundario() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getHomeSears')
            .then((response) => {
                response.data.data.forEach((element, i) => {
                    if (element.widget === "secondarySlider") {
                        dispatch({ type: SHOW_HOME_SLIDERSECUNDARIO, payload: response.data.data[i].object })
                    }
                });
            })
    }
}

// console.log("for: ", element.widget, " indice: ", i)
export function showHomeSliderTres() {
    return (dispatch, getState) => {
        axios.get('http://clauluna.com/CLARO/api/getHomeSears')
            .then((response) => {
                        dispatch({ type: SHOW_HOME_SLIDERTRES, payload: response.data.data })
            })
    }
}