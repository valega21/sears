import { combineReducers } from 'redux'
import { showMarcas1 } from './reducerMarcas'
import { showTiendas1 } from './reducerTiendas'
import { showGetHelp1 } from './reducerGetHelp'
import { showCombo1 } from './reducerContacto-formulario'
import { showAtencion1 } from './reducerAtencionClientes'
import { showGetHelpDesc1 } from './reducerGetHelpDesc'
import { showHome1 } from './reducerHome'
import { showCredito1 } from './reducerCreditoSears'
import { showProductoPolo1 } from './reducerDetalleProducto'
import { sessionReducer } from 'redux-react-session'
import { cambiarTalla } from './reducerModalTalla'
import { cambiarColor, colorSeleccionado } from './reducerModalColor'
import { skuDefaultSeleccionado} from './reducerSku'

const reducerRoot = combineReducers({
    home: showHome1, //pagina home
    marca: showMarcas1, //menu seccion marcas
    tienda: showTiendas1, //localizador de tiendas
    getHelp: showGetHelp1, //seccion ayuda : terminos y condiciones, formas de pago, preguntas frecuentes, 
    combo: showCombo1, //formulario contacto
    atencion: showAtencion1, //dudas y comentarios , contacto
    tipoPolitica: showGetHelpDesc1,
    credito: showCredito1, // credito sears
    producto: showProductoPolo1, //detalle producto playera polo
    session: sessionReducer,
    cambiarT: cambiarTalla, //modal click y recoge -> cambiar color y talla
    cambiarC: cambiarColor,
    colorS: colorSeleccionado,
    skuDS: skuDefaultSeleccionado,
});

export default reducerRoot