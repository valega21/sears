import { SHOW_ATENCION } from '../Actions/actionAtencionClientes';
import { SHOW_TITULOS} from '../Actions/actionGetHelp';

const defaultState = {
    list: [], list2: []
}

export function showAtencion1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_ATENCION: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_TITULOS: {
            return Object.assign({}, state, { list2: payload });
        }
        default:
            return state;
    }
}