import { SHOW_COMBO, SHOW_STATE } from '../Actions/actionContacto-formulario'

const defaultState = {
    list: [], list2: []
}

export function showCombo1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_COMBO: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_STATE: {
            return Object.assign({}, state, { list2: payload });
        }
        default:
            return state;
    }
}