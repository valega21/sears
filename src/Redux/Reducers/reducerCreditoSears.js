import { SHOW_CREDITO, SHOW_CREDITO_IMG } from '../Actions/CreditoSears/actionCreditoSears';

const defaultState = {
    list: [],
    list2: []
}

export function showCredito1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_CREDITO: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_CREDITO_IMG: {
            return Object.assign({}, state, { list2: payload });
        }
        default:
            return state;
    }
}