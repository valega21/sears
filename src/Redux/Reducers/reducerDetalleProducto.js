import {
    SHOW_PRODUCTO_POLO,
    SHOW_PRODUCTO,
    SHOW_PRODUCTO_PAGOS,
    SHOW_POLITICA_PRODUCTO,
    SHOW_PRODUCTO_ATRIBUTO,
    SHOW_PRODUCTO_SIMILARES,
    SHOW_PRODUCTO_IMAGENES,
    SHOW_PROMOCION_TARJETA,
    SHOW_PROMOCION,
    SHOW_COLLECTIONS,
    SHOW_SHIPPING,
    SHOW_MARCA,
    SHOW_TIPO_PRODUCTO,
    SHOW_VENDIDO_TIENDA,
    SHOW_DISPONIBILIDAD_TIENDA,
    SHOW_COLOR,
    SHOW_TALLADEFAULT
} from '../Actions/DetalleProducto/productos'

export const defaultState = {
    list: [], list2: [], list3: [], list4: [], list5: [],
    list6: [], list7: [], list8: [], list9: [], list10: [],
    list11: [], list12: [], list13: [], list14: [], list15: [], list16: [], list17: []
}

export function showProductoPolo1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_PRODUCTO_POLO: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_PRODUCTO: { //talla color
            return Object.assign({}, state, { list2: payload });
        }
        case SHOW_PRODUCTO_PAGOS: { //tipos de pagos
            return Object.assign({}, state, { list3: payload });
        }
        case SHOW_POLITICA_PRODUCTO: { //politca de devolucion
            return Object.assign({}, state, { list4: payload });
        }
        case SHOW_PRODUCTO_ATRIBUTO: { //descripcion -atributo
            return Object.assign({}, state, { list5: payload });
        }
        case SHOW_PRODUCTO_SIMILARES: { //slider productos similares
            return Object.assign({}, state, { list6: payload });
        }
        case SHOW_PRODUCTO_IMAGENES: { //slider producto imagenes
            return Object.assign({}, state, { list7: payload });
        }
        case SHOW_PROMOCION_TARJETA: { //promocion tarjetas bancarias
            return Object.assign({}, state, { list8: payload });
        }
        case SHOW_PROMOCION: { //promocion tarjetas bancarias
            return Object.assign({}, state, { list9: payload });
        }
        case SHOW_COLLECTIONS: { //Imagen tarjeta sears y carrit envio
            return Object.assign({}, state, { list10: payload });
        }
        case SHOW_SHIPPING: { //envio gratis
            return Object.assign({}, state, { list11: payload });
        }
        case SHOW_MARCA: { //marca del producto
            return Object.assign({}, state, { list12: payload });
        }
        case SHOW_TIPO_PRODUCTO: { // tipo de producto vendido en tienda o internet
            return Object.assign({}, state, { list13: payload })
        }
        case SHOW_VENDIDO_TIENDA: { // vendido en tienda
            return Object.assign({}, state, { list14: payload })
        }
        case SHOW_DISPONIBILIDAD_TIENDA: { // Disponibilidad en tienda
            return Object.assign({}, state, { list15: payload })
        }
        case SHOW_COLOR: { // Disponibilidad en tienda
            return Object.assign({}, state, { list16: payload })
        }
        case SHOW_TALLADEFAULT: { // Disponibilidad en tienda
            return Object.assign({}, state, { list17: payload })
        }
        default:
            return state;
    }
}