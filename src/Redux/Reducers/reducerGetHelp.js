import {
    SHOW_GETHELP,
    SHOW_GETHELP_PREGUNTAS,
    SHOW_AUTOCENTER_IMAGES,
    SHOW_AUTOCENTER_TITULOS,
    SHOW_AUTOCENTER_TEMPLATE,
    SHOW_AUTOCENTER_SERVICIOS,
    SHOW_AUTOCENTER_DIRECTORIO
} from '../Actions/actionGetHelp';
import {
    SHOW_GETHELP_PAGOS_TARJETASEARS,
    SHOW_GETHELP_PAGOS_MONEDERO,
    SHOW_GETHELP_PAGOS_PAYPAL,
    SHOW_GETHELP_PAGOS_TARJETASCREDITO,
    SHOW_GETHELP_PAGOS_DEPOSITO,
    SHOW_GETHELP_PAGOS_EXTRANJERO
} from '../Actions/formas-pago/actionGetHelpTiposPagos';

const defaultState = {
    list: [],
    list2: [], //Preguntas
    list3: [], //tarjetas sears
    list4: [], //monedero
    list5: [], //paypal
    list6: [], //tarjetas credito
    list7: [], //deposito
    list8: [],  //extranjero
    list9: [], //autocenter titulo
    list10: [], //autocenter imagenes
    list11: [], // autocenter template
    list12: [], // autocenteer servicios
    list13: [] //autocenter directorio
}

export function showGetHelp1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_GETHELP: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_GETHELP_PREGUNTAS: {
            return Object.assign({}, state, { list2: payload });
        }
        case SHOW_GETHELP_PAGOS_TARJETASEARS: {
            return Object.assign({}, state, { list3: payload });
        }
        case SHOW_GETHELP_PAGOS_MONEDERO: {
            return Object.assign({}, state, { list4: payload });
        }
        case SHOW_GETHELP_PAGOS_PAYPAL: {
            return Object.assign({}, state, { list5: payload });
        }
        case SHOW_GETHELP_PAGOS_TARJETASCREDITO: {
            return Object.assign({}, state, { list6: payload });
        }
        case SHOW_GETHELP_PAGOS_DEPOSITO: {
            return Object.assign({}, state, { list7: payload });
        }
        case SHOW_GETHELP_PAGOS_EXTRANJERO: {
            return Object.assign({}, state, { list8: payload });
        }
        case SHOW_AUTOCENTER_TITULOS: {
            return Object.assign({}, state, { list9: payload });
        }
        case SHOW_AUTOCENTER_IMAGES: {
            return Object.assign({}, state, { list10: payload });
        }
        case SHOW_AUTOCENTER_TEMPLATE: {
            return Object.assign({}, state, { list11: payload });
        }
        case SHOW_AUTOCENTER_SERVICIOS: {
            return Object.assign({}, state, { list12: payload });
        }
        case SHOW_AUTOCENTER_DIRECTORIO: {
            return Object.assign({}, state, { list13: payload });
        }
        default:
            return state;
    }
}