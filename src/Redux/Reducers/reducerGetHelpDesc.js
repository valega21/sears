import {
    SHOW_GETHELPDESC,
    SHOW_GETHELPDESC_TARJETASEARS,
    SHOW_GETHELPDESC_MONEDERO,
    SHOW_GETHELPDESC_PAYPAL,
    SHOW_GETHELPDESC_TARJETASCREDITO,
    SHOW_GETHELPDESC_DEPOSITO,
    SHOW_GETHELPDESC_EXTRANJERO
} from '../Actions/actionGetHelpDesc';
import { SHOW_TITULOS } from '../Actions/actionPoliticas-titulos';


const defaultState = {
    list: [],
    list2: [],
    list3: [],
    list4: [],
    list5: [],
    list6: [],
    list7: [],
    list8: []
}

export function showGetHelpDesc1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_GETHELPDESC: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_TITULOS: {
            return Object.assign({}, state, { list2: payload });
        }
        case SHOW_GETHELPDESC_TARJETASEARS: {
            return Object.assign({}, state, { list3: payload });
        }
        case SHOW_GETHELPDESC_MONEDERO: {
            return Object.assign({}, state, { list4: payload });
        }
        case SHOW_GETHELPDESC_PAYPAL: {
            return Object.assign({}, state, { list5: payload });
        }
        case SHOW_GETHELPDESC_TARJETASCREDITO: {
            return Object.assign({}, state, { list6: payload });
        }
        case SHOW_GETHELPDESC_DEPOSITO: {
            return Object.assign({}, state, { list7: payload });
        }
        case SHOW_GETHELPDESC_EXTRANJERO: {
            return Object.assign({}, state, { list8: payload });
        }
        default:
            return state;
    }
}