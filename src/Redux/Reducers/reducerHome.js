import { SHOW_HOME_SLIDERPRINCIPAL, SHOW_HOME_SLIDERSECUNDARIO, SHOW_HOME_SLIDERTRES } from '../Actions/home/actionHome';

const defaultState = {
    list: [],
    list2: [],
    list3: []
}

export function showHome1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_HOME_SLIDERPRINCIPAL: {
            return Object.assign({}, state, { list: payload });
        }
        case SHOW_HOME_SLIDERSECUNDARIO: {
            return Object.assign({}, state, { list2: payload });
        }
        case SHOW_HOME_SLIDERTRES: {
            return Object.assign({}, state, { list3: payload });
        }
        default:
            return state;
    }
}