import { SHOW_MARCAS } from '../Actions/actionMarcas';

const defaultState = {
    list: []
}

export function showMarcas1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_MARCAS: {
            return Object.assign({}, state, { list: payload });
        }
        default:
            return state;
    }
}