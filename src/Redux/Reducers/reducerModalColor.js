import {
    CAMBIAR_COLOR,
    COLOR_SELECCIONADO
} from '../Actions/DetalleProducto/actionModalColor'

var defaultState = {
    color: "",
    cambiarColor: ""
};

export function cambiarColor(state = defaultState, action) {
    switch (action.type) {
        case CAMBIAR_COLOR:
            return Object.assign({}, state, { color: action.payload });
        default:
            return state
    }
}

export function colorSeleccionado(state = defaultState, action) {
    switch (action.type) {
        case COLOR_SELECCIONADO:
            return Object.assign({}, state, { cambiarColor : action.payload });
        default:
            return state
    }
}
