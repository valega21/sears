import { CAMBIAR_TALLA } from '../Actions/DetalleProducto/modal'

const defaultState = {
  talla: ""
};

export function cambiarTalla(state = defaultState, action) {
  switch (action.type) {
    case CAMBIAR_TALLA:
      return Object.assign({}, state, { talla: action.payload });
    default:
      return state
  }
}
