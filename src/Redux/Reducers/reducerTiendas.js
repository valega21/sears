import { SHOW_TIENDAS } from '../Actions/actionTiendas';

const defaultState = {
    list: []
}

export function showTiendas1(state = defaultState, { type, payload }) {
    switch (type) {
        case SHOW_TIENDAS: {
            return Object.assign({}, state, { list: payload });
        }
        default:
            return state;
    }
}