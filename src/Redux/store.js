import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

//reducers
import { showMarcas1 } from './Reducers/reducerMarcas';
import { showTiendas1 } from './Reducers/reducerTiendas';
import { showAtencion1 } from './Reducers/reducerAtencionClientes';
import { showCombo1 } from './Reducers/reducerContacto-formulario';
import { showGetHelp1 } from './Reducers/reducerGetHelp';
import { showGetHelpDesc1 } from './Reducers/reducerGetHelpDesc';
import { showHome1 } from './Reducers/reducerHome';
import { showCredito1 } from './Reducers/reducerCreditoSears';
import { showProductoPolo1 } from './Reducers/reducerDetalleProducto';
import { sessionReducer } from 'redux-react-session';
import { sessionService } from 'redux-react-session';
import { cambiarTalla } from './Reducers/reducerModalTalla'
import { cambiarColor, colorSeleccionado } from './Reducers/reducerModalColor'
import { skuDefaultSeleccionado } from './Reducers/reducerSku'

// import tryCatch from "./Middleware/errors";

// function errorHandler(error) {
//     // acá hacemos algo con el error como mostrarlo en consola
//     // o mejor aún mandarlo a algún sistema como Sentry o track:js
//     // ¡incluso a nuestro propio servicio interno!
//     console.error(error);
//   }

const reducerRoot = combineReducers({
    state: (state = {}) => state,
    home: showHome1,
    marca: showMarcas1,
    tienda: showTiendas1,
    getHelp: showGetHelp1,
    combo: showCombo1,
    atencion: showAtencion1,
    tipoPolitica: showGetHelpDesc1,
    credito: showCredito1,
    producto: showProductoPolo1,
    session: sessionReducer,
    cambiarT: cambiarTalla,
    cambiarC: cambiarColor,
    colorS: colorSeleccionado,
    skuDS: skuDefaultSeleccionado,
});

sessionService.initSessionService(reducerRoot);
const composeEnhacers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export const store = createStore(reducerRoot, composeEnhacers(applyMiddleware(thunk //,tryCatch(errorHandler)
)))


