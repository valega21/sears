// Dependencies
import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import Breadcrumbs from 'react-router-dynamic-breadcrumbs'
import PropTypes from 'prop-types'

// Components
import App from './App';
import { store } from './Redux/store.js'
import Home from './Components/Home/HomeComponent'
import CPreguntas from './Components/SeccionAyuda/ContenedorAyudaComponent'
import CPoliticas from './Components/SeccionPoliticas/ContenedorPoliticasComponent'
import Ccontactanos from './Components/SeccionContacto/ContenedorContactoComponent'
import Ctiendas from './Components/SeccionAcercadeNosotros/ContenedorAcercadeNosotrosComponent'
import Ccredito from './Components/CreditoSears/ContenedorCredito/ContenedorCreditoComponent'
import Cmarcas from './Components/Tienda/ContenedorTiendaComponent'
import DetalleProducto from './Components/Productos/DetalleProductoComponent'
import Login from './Components/Login/LoginComponent'
import Recuperar from './Components/Login/Recuperar/RecuperarComponent'
import Registro from './Components/Login/Registro/RegistroComponent'

const routes = {
  '/': 'Inicio',
  '/localizador-tiendas': 'Acerca de Nosotros',
  '/acerca-de-nosotros': 'Acerca de Nosotros',
  '/terminos-y-condiciones': 'Términos y Condiciones',
  '/Aviso_Privacidad': 'Aviso de privacidad clientes',
  '/aviso-privacidad-prospectos': 'Aviso de privacidad prospectos',
  '/politicas-de-cancelacion': 'Políticas de cancelación',
  '/politicas-de-devolucion': 'Políticas de devolución',
  '/politicas-de-envio': 'Politicas de envío',
  '/politicas-de-compra': 'Políticas de compra',
  '/dudas-y-comentarios': 'Dudas y Comentarios',
  '/formas-pago': 'Formas de Pago',
  '/preguntas-frecuentes': 'Preguntas Frecuentes',
  '/auto-center': 'AutoCenter',
  '/contactanos': 'Contáctanos',
  '/tienda/3451/pier-1-imports': 'Pier 1 Imports',
  '/credito-sears/beneficios': 'Beneficios',
  '/credito-sears/puntos-sears': 'Puntos Sears',
  '/credito-sears/preguntas-frecuentes': 'Preguntas Frecuentes',
  '/credito-sears/descuento-primera-compra': 'Descuento Primera Compra',
  '/credito-sears/carta-cumpleanos': 'Carta Cumpleaños',
  '/credito-sears/cupon-vip': 'Cupón VIP',
  '/credito-sears/requisitos-y-formato-de-solicitud': 'Requisitos y Formato de Solicitud'
}

const AppRoutes = ({ authenticated, checked }) =>
  <Provider store={store}>
    <App >
      {(window.location.pathname === '/login' || window.location.pathname === "/loginregistro")
        ? null
        : <Breadcrumbs mappedRoutes={routes} />
      }

      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/recuperar" component={Recuperar} />
        <Route exact path="/loginregistro" component={Registro} />

        {/* Detalle Producto */}
        <Route exact strict path="/producto/:id/" component={DetalleProducto} />

        {/* Sección Créditos Sears */}
        <Route exact path="/credito-sears/beneficios" component={Ccredito} />
        <Route exact path="/credito-sears/beneficios" component={Ccredito} />
        <Route exact path="/credito-sears/puntos-sears" component={Ccredito} />
        <Route exact path="/credito-sears/preguntas-frecuentes" component={Ccredito} />
        <Route exact path="/credito-sears/descuento-primera-compra" component={Ccredito} />
        <Route exact path="/credito-sears/carta-cumpleanos" component={Ccredito} />
        <Route exact path="/credito-sears/cupon-vip" component={Ccredito} />
        <Route exact path="/credito-sears/requisitos-y-formato-de-solicitud" component={Ccredito} />

        {/* Seccion Nuestra Compañia */}
        <Route exact path="/localizador-tiendas" component={Ctiendas} />
        <Route exact path="/acerca-de-nosotros" component={Ctiendas} />

        {/* seccion marcas */}
        <Route exact path="/tienda/3451/pier-1-imports" component={Cmarcas} />

        {/* Seccion Politicas */}
        <Route exact name="Terminos y Condiciones" path="/terminos-y-condiciones" component={CPoliticas} />
        <Route exact path="/Aviso_Privacidad" component={CPoliticas} />
        <Route exact path="/aviso-privacidad-prospectos" component={CPoliticas} />
        <Route exact path="/politicas-de-cancelacion" component={CPoliticas} />
        <Route exact path="/politicas-de-devolucion" component={CPoliticas} />
        <Route exact path="/politicas-de-envio" component={CPoliticas} />
        <Route exact path="/politicas-de-compra" component={CPoliticas} />

        {/* Seccion Déjanos Ayudarte */}
        <Route exact path="/dudas-y-comentarios" component={CPreguntas} />
        <Route exact path="/preguntas-frecuentes" component={CPreguntas} />
        <Route exact path="/formas-pago" component={CPreguntas} />
        <Route exact path="/auto-center" component={CPreguntas} />

        {/* Seccion Contáctanos */}
        <Route exact path="/contactanos" component={Ccontactanos} />

        {/* <Route component={Page404} /> */}
      </Switch>
    </App>
  </Provider>

const { bool } = PropTypes;
AppRoutes.propTypes = {
  authenticated: bool.isRequired,
  checked: bool.isRequired
};

// const mapState = ({ session }) => ({
//   checked: session.checked,
//   authenticated: session.authenticated
// });

export default AppRoutes
